package com.example.healthily.Adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.healthily.Models.Articles
import com.example.healthily.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.news_item.view.*

/**
 * View holder for a news item.
 */
class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

/**
 * Adapter for the news recycler view.
 */
class NewsRecyclerAdapter(val context: Context, val news: List<Articles>) :
    RecyclerView.Adapter<NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.news_item,
                parent,
                false
            ) as ConstraintLayout
        )
    }

    override fun getItemCount() = news.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val article = news[position]
        holder.itemView.news_title.text = article.title
        holder.itemView.news_image.clipToOutline = true
        Picasso.get().load(article.urlToImage).into(holder.itemView.news_image)

        holder.itemView.setOnClickListener {
            openUrl(article.url)
        }

    }

    /**
     * Retrieve news from a URI string.
     */
    fun openUrl(uri: String) {
        val openURL = Intent(Intent.ACTION_VIEW)
        openURL.data = Uri.parse(uri)
        startActivity(context, openURL, null)
    }
}
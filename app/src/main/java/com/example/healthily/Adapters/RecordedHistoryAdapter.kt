package com.example.healthily.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.healthily.Controllers.FitnessFragment
import com.example.healthily.Models.History
import com.example.healthily.R
import kotlinx.android.synthetic.main.recorded_history_item.view.*

/**
 * Adapter for the recycler view holding recorded data history.
 */
class RecordedHistoryRecyclerAdapter() :
    RecyclerView.Adapter<NewsViewHolder>() {

    /**
     * Call this when the user taps on a history item.
     */
    private lateinit var adapterCallback: AdapterCallback
    private lateinit var history: List<History>

    constructor(ctx: FitnessFragment, history: List<History>) : this() {

        this.adapterCallback = ctx
        this.history = history
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recorded_history_item,
                parent,
                false
            ) as ConstraintLayout
        )
    }

    override fun getItemCount() = history.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val item = history[position]
        holder.itemView.tv_date.text = item.title
        holder.itemView.tv_km.text = item.more

        holder.itemView.setOnClickListener {
            adapterCallback.onMethodCallback(item)
        }
    }

    /**
     * Represents an event triggered when the user taps on an history item.
     */
    interface AdapterCallback {
        fun onMethodCallback(item: History)
    }
}

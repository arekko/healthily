package com.example.healthily.Controllers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.healthily.Adapters.ViewPagerAdapter
import com.example.healthily.R
import kotlinx.android.synthetic.main.activity_detail.*

/**
 * Activity that presents the details of a recorded history item (steps, running tracks, ...).
 */
class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // Parameters to determine which data to show and how to present it
        val startT = intent.extras?.getDouble("startT", 0.0)
        val endT = intent.extras?.getDouble("endT", 0.0)
        val hasMap = intent.extras?.getBoolean("hasMap", true)
        val type = if (hasMap != null && hasMap) GraphFragment.GraphType.RUNNING.id else GraphFragment.GraphType.STEPS.id

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(GraphFragment.newInstance(type, startT ?: 0.0, endT ?: 0.0), "Graph")

        // Check if the map fragment is supposed to be visible (if it's running track)
        if (hasMap != null && hasMap) {
            adapter.addFragment(MapFragment.newInstance(startT ?: 0.0, endT ?: 0.0), "Map")
        }

        viewPager.adapter = adapter
        tabs_main.setupWithViewPager(viewPager)
    }
}

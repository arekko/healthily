package com.example.healthily.Controllers


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.healthily.Adapters.RecordedHistoryRecyclerAdapter
import com.example.healthily.Models.History
import kotlinx.android.synthetic.main.fragment_fitness.*
import kotlinx.android.synthetic.main.fragment_fitness.view.*
import org.jetbrains.anko.support.v4.startActivity
import com.example.healthily.Models.Data
import com.example.healthily.R
import com.example.healthily.Utils.ValueStream
import com.example.healthily.Utils.ValueStreamIterator
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.YearMonth
import java.util.*
import kotlin.math.min


const val NESTING_DEPTH = 2

/**
 * Fragment to navigate the running/steps history.
 */
class FitnessFragment : Fragment(), RecordedHistoryRecyclerAdapter.AdapterCallback {

    /**
     * How deep are we in the navigation menu (all, month, day, ...)
     */
    private var depthCounter: Int = 0

    /**
     * The maximum depth after which tapping on an element opens DetailActivity
     */
    private var maxDepth: Int = NESTING_DEPTH

    /**
     * The type of history to be shown in this fragment.
     */
    private var historyType: HistoryType = HistoryType.RUNNING

    /**
     * The stack containing the parent history items from which we came to the
     * current item.
     */
    private var historyStack = Stack<History>()

    lateinit var actionBar: ActionBar
    lateinit var adapter: RecordedHistoryRecyclerAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Retrieve the arguments of the Fragment
        arguments?.getBoolean("hasTracks")?.let {
            maxDepth = if (it) NESTING_DEPTH + 1 else NESTING_DEPTH
            historyType = if (it) HistoryType.RUNNING else HistoryType.STEPS
        }
    }

    override fun onResume() {
        super.onResume()
        // If there is a saved state of the correct history type
        if (Data.TemporaryState.CurrentHistoryItem.savedState &&
            historyType == Data.TemporaryState.CurrentHistoryItem.historyType) {

            // Reload the state
            historyStack = Data.TemporaryState.CurrentHistoryItem.historyStack.clone() as Stack<History>
            depthCounter = Data.TemporaryState.CurrentHistoryItem.depthCounter
            maxDepth = Data.TemporaryState.CurrentHistoryItem.maxDepth

            // Remove the saved state
            Data.TemporaryState.CurrentHistoryItem.savedState = false

            // Set the title according to the current history item
            if (historyStack.empty()) {
                resetTitle()
            } else {
                val currentItem = peekHistoryStack()
                if (currentItem != null) {
                    setTitle(currentItem.title)
                } else {
                    resetTitle()
                }
            }

            // Retrieve history data from the ValueStreams
            val newData = retrieveData()

            // Update the navigation view with the specified data
            updateData(newData)

            // Don't show the home button if we are viewing the whole histoy
            if (this.depthCounter != 0) {
                actionBar.setHomeButtonEnabled(true)
                actionBar.setDisplayHomeAsUpEnabled(true)
            } else {
                actionBar.setHomeButtonEnabled(false)
                actionBar.setDisplayHomeAsUpEnabled(false)
            }

        } else {
            // If there is no saved state, start from scratch
            historyStack.clear()
            resetTitle()
            val newData = retrieveData()
            updateData(newData)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Save the state of the navigation view
        Data.TemporaryState.CurrentHistoryItem.historyStack = historyStack.clone() as Stack<History>
        Data.TemporaryState.CurrentHistoryItem.depthCounter = depthCounter
        Data.TemporaryState.CurrentHistoryItem.maxDepth = maxDepth
        Data.TemporaryState.CurrentHistoryItem.historyType = historyType
        Data.TemporaryState.CurrentHistoryItem.savedState = true
    }

    override fun onMethodCallback(item: History) {
        // If we are at the bottom of the hierarchy, start the DetailsActivity on the tapped item
        if (this.depthCounter == maxDepth - 1) {
            startDetailActivity(item)
            return
        }

        // If we are not at the bottom, update the state and refresh the data
        this.depthCounter++
        historyStack.push(item)
        setTitle(item.title)
        val newData = retrieveData()
        updateData(newData)

        // When we are not at the top, the home button should be visible
        if (this.depthCounter != 0) {
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_chart, menu)
        // Add content description for accessibility
        menu.findItem(R.id.btn_chart)?.contentDescription = getString(R.string.show_graph)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.btn_chart) {
            // When clicking on the graph icon, show the DetailActivity for the current history item
            val history = peekHistoryStack()
            if (history != null) {
                startDetailActivity(history)
            } else {
                val (first, last) = when (historyType) {
                    HistoryType.RUNNING ->
                        Pair(Data.Running.geoStream?.data?.firstT ?: 0.0,
                            Data.Running.geoStream?.data?.lastT ?: 0.0)
                    HistoryType.STEPS ->
                        Pair(Data.Steps.stepStream.firstT ?: 0.0, Data.Steps.stepStream.lastT ?: 0.0)
                }
                startDetailActivity(History("Everything", "", first, last))
            }
        } else {
            // When clicking on the home button, go back to the previous item
            this.depthCounter--
            if (historyStack.empty()) {
                resetTitle()
            } else {
                historyStack.pop()
                val currentItem = peekHistoryStack()
                if (currentItem != null) {
                    setTitle(currentItem.title)
                } else {
                    resetTitle()
                }
            }

            // Retrieve the data for the previous item and update the view
            val newData = retrieveData()
            updateData(newData)

            // If we reached the top of the hierarchy, hide the home button
            if (this.depthCounter == 0) {
                actionBar.setHomeButtonEnabled(false)
                actionBar.setDisplayHomeAsUpEnabled(false)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =
            inflater.inflate(com.example.healthily.R.layout.fragment_fitness, container, false)
        this.actionBar = (activity as AppCompatActivity).supportActionBar!!

        setHasOptionsMenu(true)

        // Show the fab button (add a track) only in the running history, not in the step history
        if (historyType == HistoryType.RUNNING) {
            view.fitness_fab.show()
            view.fitness_fab.setOnClickListener {
                startActivity<TrackerActivity>()
            }
        } else {
            view.fitness_fab.hide()
        }

        view.fitness_recycler_view.layoutManager = LinearLayoutManager(context)

        // Retrieve the data for the first time and populate the view
        val data = retrieveData()
        adapter = RecordedHistoryRecyclerAdapter(this, data)
        view.fitness_recycler_view.adapter = adapter

        return view
    }

    /**
     * Start the DetailActivity and set it to show the data of the specified history item.
     */
    private fun startDetailActivity(history: History) {
        val hasMap = when (historyType) {
            HistoryType.RUNNING -> true
            HistoryType.STEPS -> false
        }
        val intent = Intent(context, DetailActivity::class.java)
        val bundle = Bundle()
        bundle.putBoolean("hasMap", hasMap)
        bundle.putDouble("startT", history.startT)
        bundle.putDouble("endT", history.endT)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    /**
     * Update the view given a list of history data.
     */
    private fun updateData(data: List<History>) {
        adapter = RecordedHistoryRecyclerAdapter(this, data)
        fitness_recycler_view.adapter = adapter
    }

    /**
     * Set the header title of the fragment to the default value (at the top pf the hierarchy)
     */
    private fun resetTitle() {
        actionBar.title = when (historyType) {
            HistoryType.RUNNING -> getString(R.string.menu_fitness)
            HistoryType.STEPS -> getString(R.string.menu_steps)
        }
    }

    /**
     * Set the header title of the fragment.
     */
    private fun setTitle(title: String) {
        actionBar.title = title
    }

    /**
     * Peek the current history element from the history stack. If the stack is empty return null.
     */
    private fun peekHistoryStack(): History? {
        return try {historyStack.peek()} catch (ex: Exception) {null}
    }

    /**
     * Retrieve history data from the appropriate ValueStream.
     */
    private fun retrieveData(): List<History> {
        return retrieveTimeIntervals(
            when (historyType) {
                HistoryType.RUNNING -> Data.Running.geoStream?.distance ?: ValueStream.empty()
                HistoryType.STEPS -> Data.Steps.stepStream
            },
            depthCounter,
            peekHistoryStack()?.startT ?: 0.0
        )
    }

    /**
     * Retrieve the appropriate history from a ValueStream, given a beginning point and a depth, which defines the scope
     * of the data (all, month, day, track, ...).
     */
    private fun <T: Any> retrieveTimeIntervals(stream: ValueStream<T>, depth: Int, beginningT: Double): List<History> {
        val result: MutableList<History> = mutableListOf()
        if (depth == 0) {
            return getAllHistories(stream)
        } else if (depth == 1) {
            return getMonthHistories(stream, beginningT)
        } else if (depth == 2) {
            return getDayHistories(beginningT)
        }
        return result
    }

    /**
     * Get all the monthly histories in this ValueStream.
     */
    private fun <T: Any> getAllHistories(stream: ValueStream<T>): List<History> {
        val result: MutableList<History> = mutableListOf()
        val firstT = stream.firstT
        val lastT = stream.lastT

        if (firstT != null && lastT != null) {
            // Get the first and last months of the stream
            val timeFirst =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(firstT.toLong()), TimeZone.getDefault().toZoneId())
            val startYear = timeFirst.year
            val startMonth = timeFirst.month

            val timeLast =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(lastT.toLong()), TimeZone.getDefault().toZoneId())
            val endYear = timeLast.year
            val endMonth = timeLast.month

            var currentYearMonth = YearMonth.of(startYear, startMonth)
            val endYearMonth = YearMonth.of(endYear, endMonth)

            // Iterate through all the months in between
            while (currentYearMonth <= endYearMonth) {
                // We use capitalize() instead of capitalize(Locale) because the latter is experimental
                val monthName =
                    currentYearMonth.month.name.toLowerCase(Locale.ENGLISH).capitalize()
                val yearName = currentYearMonth.year.toString()
                // Get the start and end timestamps of the current month
                val startT = currentYearMonth
                    .atDay(1)
                    .atStartOfDay()
                    .atZone(TimeZone.getDefault().toZoneId())
                    .toInstant()
                    .toEpochMilli()
                    .toDouble()
                val endT = currentYearMonth
                    .plusMonths(1)
                    .atDay(1)
                    .atStartOfDay()
                    .atZone(TimeZone.getDefault().toZoneId())
                    .toInstant()
                    .toEpochMilli()
                    .toDouble()
                val doubleStream = when (historyType) {
                    HistoryType.RUNNING -> Data.Running?.geoStream?.distance
                    HistoryType.STEPS  -> Data.Steps.stepStream
                }

                // Retrieve the total qquantity (distance or steps) for the current month
                val quantity = getTotalQuantity(doubleStream, startT, endT)
                val more = getMoreString(quantity)

                // Create the History item with the information retrieved about the month
                result.add(History("$monthName $yearName", more, startT, endT))

                currentYearMonth = currentYearMonth.plusMonths(1)
            }
        }

        // Reverse the history, so newer entries are shown first
        return result.reversed()
    }

    /**
     * Get all the monthly histories in this ValueStream in the month where beginningT belongs.
     */
    private fun <T: Any> getMonthHistories(stream: ValueStream<T>, beginningT: Double): List<History> {
        val result: MutableList<History> = mutableListOf()

        // Find the first and last day of the month beginningT belongs to
        val timeFirst =
            LocalDateTime.ofInstant(Instant.ofEpochMilli(beginningT.toLong()), TimeZone.getDefault().toZoneId())
        val startYear = timeFirst.year
        val startMonth = timeFirst.month
        val startDay = timeFirst.dayOfMonth
        val firstDate = LocalDate.of(startYear, startMonth, startDay)

        val lastT = min(stream.lastT ?: Double.MAX_VALUE, firstDate
            .plusMonths(1)
            .atStartOfDay()
            .atZone(TimeZone.getDefault().toZoneId())
            .toInstant()
            .toEpochMilli()
            .toDouble() - 1.0)

        val timeLast =
            LocalDateTime.ofInstant(Instant.ofEpochMilli(lastT.toLong()), TimeZone.getDefault().toZoneId())
        val endYear = timeLast.year
        val endMonth = timeLast.month
        val endDay = timeLast.dayOfMonth
        val lastDate = LocalDate.of(endYear, endMonth, endDay)

        var currentDate = firstDate

        // Iterate through all the dates in the month
        while (currentDate <= lastDate) {
            // Find the first and last timestamps for each date
            val startT = currentDate
                .atStartOfDay()
                .atZone(TimeZone.getDefault().toZoneId())
                .toInstant()
                .toEpochMilli()
                .toDouble()

            val endT = min(stream.lastT ?: Double.MAX_VALUE, currentDate
                .plusDays(1)
                .atStartOfDay()
                .atZone(TimeZone.getDefault().toZoneId())
                .toInstant()
                .toEpochMilli()
                .toDouble())

            // We use capitalize() instead of capitalize(Locale) because the latter is experimental
            val dayName =
                currentDate.dayOfWeek.name.toLowerCase(Locale.ENGLISH).capitalize() + " " +
                        getOrdinal(currentDate.dayOfMonth)

            val doubleStream = when (historyType) {
                HistoryType.RUNNING -> Data.Running?.geoStream?.distance
                HistoryType.STEPS  -> Data.Steps.stepStream
            }

            val quantity = getTotalQuantity(doubleStream, startT, endT)
            val more = getMoreString(quantity)

            // Create the History item with the information retrieved about the day
            result.add(History(dayName, more, startT, endT))

            currentDate = currentDate.plusDays(1)
        }
        return result.reversed()
    }

    /**
     * Get the histories for all the tracks in the day where beginingT belongs.
     */
    private fun getDayHistories(beginningT: Double): List<History> {
        val result: MutableList<History> = mutableListOf()
        val timeFirst =
            LocalDateTime.ofInstant(Instant.ofEpochMilli(beginningT.toLong()), TimeZone.getDefault().toZoneId())
        val startYear = timeFirst.year
        val startMonth = timeFirst.month
        val startDay = timeFirst.dayOfMonth

        // Get the first and last timestamp in the day
        val firstTime =
            LocalDateTime.of(startYear, startMonth, startDay, 0, 0, 0)
        val lastTime =
            LocalDateTime.of(startYear, startMonth, startDay, 23, 59, 59)
        val firstT = firstTime
            .atZone(TimeZone.getDefault().toZoneId())
            .toInstant()
            .toEpochMilli()
            .toDouble()
        val lastT = lastTime
            .atZone(TimeZone.getDefault().toZoneId())
            .toInstant()
            .toEpochMilli()
            .toDouble()

        // The "Tracks" level is only supported for Running, so use directly GeoStream
        val subStream =
            Data.Running?.geoStream?.data?.section(firstT, lastT) ?: ValueStream.empty()
        val iterator = subStream.iterator()
        val samples = iterator.asSequence().toList()

        // Find the index of the first track, to compute the track number within a day
        val firstTrackIndex =
            (samples.firstOrNull() as ValueStreamIterator.Sample.Value?)?.value?.trackIndex ?: 0

        // Find all the tracks between first and last
        val tracks = samples
            .filter { it is ValueStreamIterator.Sample.Value }
            .groupBy { (it as ValueStreamIterator.Sample.Value).value.trackIndex }
            .map {
                Pair(it.key,
                    Pair((it.value.firstOrNull() as ValueStreamIterator.Sample.Value?)?.time,
                        (it.value.lastOrNull() as ValueStreamIterator.Sample.Value?)?.time))}

        // Produce an history for all the tracks
        for (track in tracks) {
            if (track.second.first != null && track.second.second != null) {
                val trackNumber = track.first - firstTrackIndex + 1

                val doubleStream = when (historyType) {
                    HistoryType.RUNNING -> Data.Running?.geoStream?.distance
                    HistoryType.STEPS -> Data.Steps.stepStream
                }

                val quantity =
                    getTotalQuantity(doubleStream, track.second.first!!, track.second.second!!)
                val more = getMoreString(quantity)

                result.add(History("Track $trackNumber", more, track.second.first!!,
                    track.second.second!!))
            }
        }

        return result
    }

    /**
     * Get the additional info string associated with a quantity. It includes
     * the units and it's properly rounded.
     */
    private fun getMoreString(quantity: Double): String {
        return String.format("%.2f", when (historyType) {
            HistoryType.RUNNING -> quantity / 1000.0
            HistoryType.STEPS -> quantity
        }) + when (historyType) {
            HistoryType.RUNNING -> " km"
            HistoryType.STEPS -> " steps"
        }
    }

    /**
     * Get an ordinal from a cardinal. Valid up to 99.
     */
    private fun getOrdinal(cardinal: Int): String {
        val units = cardinal % 10
        val strNum = cardinal.toString()
        return when (units) {
            1 -> strNum + "st"
            2 -> strNum + "nd"
            3 -> strNum + "rd"
            else -> strNum + "th"
        }
    }

    /**
     * Get the total of a quantity between two timestamps from a cumulative ValueStream.
     */
    private fun getTotalQuantity(stream: ValueStream<Double>?, startT: Double, endT: Double): Double {
        val startValue = stream?.firstAfter(startT)
        val endValue = stream?.firstBefore(endT)

        if (startValue != null && endValue != null && startValue < endValue) {
            return endValue - startValue
        } else {
            return 0.0
        }
    }

    companion object {
        /**
         * To create a new instance of this fragment and passing parameters to it.
         */
        @JvmStatic
        fun newInstance(isRunning: Boolean) = FitnessFragment().apply {
            arguments = Bundle().apply {
                putBoolean("hasTracks", isRunning)
            }
        }
    }

    /**
     * The type of history this fragment will show.
     */
    enum class HistoryType {
        RUNNING,
        STEPS
    }
}

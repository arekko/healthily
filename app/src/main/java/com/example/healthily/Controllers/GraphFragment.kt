package com.example.healthily.Controllers


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.healthily.Models.Data
import com.example.healthily.R
import com.example.healthily.Utils.ValueStream
import com.example.healthily.Utils.ValueStreamIterator
import com.example.healthily.Utils.derivative
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.fragment_graph.view.*

/**
 * A fragment presenting a graph of a section of a ValueStream containing speeds,
 * steps or distances.
 */
class GraphFragment : Fragment() {

    /**
     * The starting timestamp of the graph's data.
     */
    private var startT: Double? = null

    /**
     * The ending tumestamp of the graph's data.
     */
    private var endT: Double? = null

    /**
     * The type of data being displayed (running data or steps).
     */
    private var type: GraphType = GraphType.RUNNING

    /**
     * The quantity on the x-axis if the type of the graph is running data.
     */
    private var xAxisRunning: XAxisRunning = XAxisRunning.TIME

    /**
     * The quantity on the x-axis if the type of the graph is steps data.
     */
    private var xAxisSteps: XAxisSteps = XAxisSteps.TIME

    /**
     * The quantity on the y-axis if the type of the graph is running data.
     */
    private var yAxisRunning: YAxisRunning = YAxisRunning.SPEED

    /**
     * The quantity on the y-axis if the type of the graph is steps data.
     */
    private var yAxisSteps: YAxisSteps = YAxisSteps.STEPS

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Retrieve the parameters for the fragment
        arguments?.getDouble("startT")?.let {
            startT = it
        }
        arguments?.getDouble("endT")?.let {
            endT = it
        }
        arguments?.getString("type")?.let {
            type = when(it) {
                "steps" -> GraphType.STEPS
                else -> GraphType.RUNNING
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_graph, container, false)

        /*
         * Retrieve the choices for the type of quantity to show in the graph and set them to the
         * spinners for the X and Y axis.
         */

        val spinnerXElems = when(type) {
            GraphType.RUNNING -> XAxisRunning.values().map { it.name }.toTypedArray()
            GraphType.STEPS -> XAxisSteps.values().map { it.name }.toTypedArray()
        }

        val spinnerYElems = when(type) {
            GraphType.RUNNING -> YAxisRunning.values().map { it.name }.toTypedArray()
            GraphType.STEPS -> YAxisSteps.values().map { it.name }.toTypedArray()
        }

        val spinnerXAdapter = ArrayAdapter<String>(
            activity!!,
            R.layout.spinner_item,
            spinnerXElems)

        val spinnerYAdapter = ArrayAdapter<String>(
            activity!!,
            R.layout.spinner_item,
            spinnerYElems)

        spinnerXAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinnerYAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)

        view.xAxisSpinner.adapter = spinnerXAdapter
        view.yAxisSpinner.adapter = spinnerYAdapter

        spinnerXAdapter.notifyDataSetChanged()
        spinnerYAdapter.notifyDataSetChanged()

        /*
         * Register the listeners to update the graph when the user picks a different quantity.
         */
        view.xAxisSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val item = p0?.getItemAtPosition(p2).toString()
                when(type) {
                    GraphType.RUNNING -> xAxisRunning = XAxisRunning.valueOf(item)
                    GraphType.STEPS -> xAxisSteps = XAxisSteps.valueOf(item)
                }
                updateGraph(view)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                // Empty
            }
        }

        /*
         * Register the listeners to update the graph when the user picks a different quantity.
         */
        view.yAxisSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val item = p0?.getItemAtPosition(p2).toString()
                when(type) {
                    GraphType.RUNNING -> yAxisRunning = YAxisRunning.valueOf(item)
                    GraphType.STEPS -> yAxisSteps = YAxisSteps.valueOf(item)
                }
                updateGraph(view)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                // Empty
            }
        }

        // Set the layout and behaviour parameters for the graph
        view.graph.viewport.isScalable = true
        view.graph.viewport.isScrollable = true

        view.graph.gridLabelRenderer.horizontalLabelsColor = Color.valueOf(0f, 0f, 0f).toArgb()
        view.graph.gridLabelRenderer.isHorizontalLabelsVisible = true
        view.graph.gridLabelRenderer.verticalLabelsColor = Color.valueOf(0f, 0f, 0f).toArgb()
        view.graph.gridLabelRenderer.isVerticalLabelsVisible = true
        view.graph.gridLabelRenderer.numHorizontalLabels = 5
        view.graph.gridLabelRenderer.numVerticalLabels = 10
        view.graph.gridLabelRenderer.horizontalAxisTitleColor = Color.valueOf(0f, 0f, 0f).toArgb()
        view.graph.gridLabelRenderer.verticalAxisTitleColor = Color.valueOf(0f, 0f, 0f).toArgb()
        view.graph.gridLabelRenderer.padding = 80
        view.graph.gridLabelRenderer.textSize = 40f
        view.graph.gridLabelRenderer.horizontalAxisTitleTextSize = 48f
        view.graph.gridLabelRenderer.verticalAxisTitleTextSize = 48f
        view.graph.gridLabelRenderer.reloadStyles()

        Toast.makeText(context, getText(R.string.graph_tip), Toast.LENGTH_LONG).show()

        // Load the data into the graph and show it
        updateGraph(view)

        return view
    }

    /**
     * Loads the data from the running/steps streams and display the section between startT and endT,
     * picking the desired quantity for each axis.
     */
    private fun updateGraph(view: View) {
        // Clean the graph
        view.graph.removeAllSeries()

        when(type) {
            GraphType.RUNNING -> {
                if (xAxisRunning == XAxisRunning.DISTANCE && yAxisRunning == YAxisRunning.DISTANCE) {
                    // The user picked the same variable for X and Y axis, nothing to show
                    view.graph.removeAllSeries()
                } else {
                    // Fetch the chosen y-axis quantity and unit
                    val (streamY, unitFactorY, yCumulative) = when(yAxisRunning) {
                        YAxisRunning.DISTANCE -> {
                            view.graph.gridLabelRenderer.verticalAxisTitle = "km"
                            Triple(
                                Data.Running.geoStream?.distance ?: ValueStream.empty(),
                                0.001, true)
                        }
                        YAxisRunning.SPEED -> {
                            view.graph.gridLabelRenderer.verticalAxisTitle = "km/h"
                            Triple(
                                Data.Running.geoStream?.speed ?: ValueStream.empty(),
                                3.6, false)
                        }
                    }
                    // Show the unit of the chosen quantity for the x-axis
                    view.graph.viewport.setMinX(0.0)
                    // Pick between "meters to km" and "milliseconds to minutes"
                    val unitFactorX = if (xAxisRunning == XAxisRunning.DISTANCE) 0.001 else 1.0 / 60000.0
                    if (xAxisRunning == XAxisRunning.DISTANCE) {
                        val distance = Data.Running.geoStream?.distance ?: ValueStream.empty()
                        val firstDistance = distance.firstAfter(startT ?: 0.0) ?: 0.0
                        val lastDistance = distance.firstBefore(endT ?: 0.0) ?: 1.0
                        view.graph.viewport.setMaxX((lastDistance - firstDistance) * unitFactorX)
                        view.graph.gridLabelRenderer.horizontalAxisTitle = "km"
                    } else {
                        view.graph.gridLabelRenderer.horizontalAxisTitle = "mins"
                        view.graph.viewport.setMaxX(((endT ?: 1.0) - (startT ?: 0.0)) * unitFactorX)
                    }


                    // Create graph DataPoint objects from the ValueStream section
                    buildDataPoints(xAxisRunning == XAxisRunning.DISTANCE, streamY,
                        unitFactorX, unitFactorY, yCumulative)
                        .forEach {
                            view.graph.addSeries(LineGraphSeries(it))
                        }
                }
            }
            GraphType.STEPS -> {
                val millisecondToHours = 1.0 / (3600.0 * 1000.0)
                view.graph.gridLabelRenderer.numHorizontalLabels = 10
                val (streamY, unitFactorY, yCumulative) = when (yAxisSteps) {
                    YAxisSteps.STEPS -> {
                        view.graph.gridLabelRenderer.verticalAxisTitle = "steps"
                        Triple(Data.Steps.stepStream, 1.0, true)
                    }
                    YAxisSteps.STEPS_PER_HOUR -> {
                        view.graph.gridLabelRenderer.verticalAxisTitle = "steps/hour"
                        Triple(Data.Steps.stepStream.derivative(), 1 / millisecondToHours, false)
                    }
                }
                view.graph.gridLabelRenderer.horizontalAxisTitle = "hours"
                view.graph.viewport.setMinX(0.0)
                view.graph.viewport.setMaxX(((endT ?: 1.0) - (startT ?: 0.0)) * millisecondToHours)
                buildDataPoints(false, streamY, millisecondToHours, unitFactorY, yCumulative)
                    .forEach {
                        view.graph.addSeries(LineGraphSeries(it))
                    }
            }
        }
        view.graph.gridLabelRenderer.reloadStyles()
    }

    /**
     * Create graph DataPoint objects from the ValueStream section, using the desired quantities.
     * Each returned Array<DataPoint> represents a contiguous section of the ValueStream.
     */
    private fun buildDataPoints(useDistanceAsX: Boolean, streamY: ValueStream<Double>,
                                unitFactorX: Double = 1.0, unitFactorY: Double = 1.0,
                                yCumulative: Boolean = false): List<Array<DataPoint>> {
        val result: MutableList<MutableList<DataPoint>> = mutableListOf()
        val section = streamY.section(startT ?: 0.0, endT ?: 0.0)
        // Do we need to subtract the first value from the y-value (i.e. is it cumulative, like distance, unlike speed)
        val firstValue = if (yCumulative) {
            streamY.firstAfter(startT ?: 0.0) ?: 0.0
        } else {
            0.0
        }
        if (useDistanceAsX) {
            // Use distance (converted to km)
            val distance = Data.Running.geoStream?.distance?.map { it * unitFactorX } ?: ValueStream.empty()
            val firstDistance = distance.firstAfter(startT ?: 0.0) ?: 0.0
            val iterator = section.iterator()
            result.add(mutableListOf())
            for (sample in iterator) {
                when (sample) {
                    is ValueStreamIterator.Sample.Value -> {
                        result.get(result.size - 1).add(
                            DataPoint((distance.valueAt(sample.time) ?: 0.0) - firstDistance,
                                (sample.value - firstValue) * unitFactorY))
                    }
                    is ValueStreamIterator.Sample.None -> {
                        if (result.isNotEmpty() && result.get(result.size - 1).isNotEmpty()) {
                            result.add(mutableListOf())
                        }
                    }
                }
            }
        } else {
            val firstTime = startT
            val iterator = section.iterator()
            result.add(mutableListOf())
            for (sample in iterator) {
                when (sample) {
                    is ValueStreamIterator.Sample.Value -> {
                        // Convert the milliseconds to minutes and pair with the respective sample
                        result.get(result.size - 1).add(
                            DataPoint(((sample.time - (firstTime ?: 0.0)) * unitFactorX),
                                (sample.value - firstValue) * unitFactorY))
                    }
                    is ValueStreamIterator.Sample.None -> {
                        if (result.isNotEmpty() && result.get(result.size - 1).isNotEmpty()) {
                            result.add(mutableListOf())
                        }
                    }
                }
            }
        }

        return result.map { it.toTypedArray() }
    }

    companion object {
        @JvmStatic
        fun newInstance(type: String, startT: Double, endT: Double) = GraphFragment().apply {
            arguments = Bundle().apply {
                putDouble("startT", startT)
                putDouble("endT", endT)
                putString("type", type)
            }
        }
    }

    /**
     * The quantity on the x-axis if the type of the graph is running data.
     */
    private enum class XAxisRunning {
        TIME,
        DISTANCE
    }

    /**
     * The quantity on the y-axis if the type of the graph is running data.
     */
    private enum class YAxisRunning {
        SPEED,
        DISTANCE
    }

    /**
     * The quantity on the x-axis if the type of the graph is step data.
     */
    private enum class XAxisSteps {
        TIME
    }

    /**
     * The quantity on the y-axis if the type of the graph is step data.
     */
    private enum class YAxisSteps {
        STEPS,
        STEPS_PER_HOUR
    }

    /**
     * The type of the graph.
     */
    enum class GraphType(val id: String) {
        RUNNING("running"),
        STEPS("steps")
    }
}

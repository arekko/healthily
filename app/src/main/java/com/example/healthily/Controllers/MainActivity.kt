package com.example.healthily.Controllers

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.healthily.Models.Data
import com.example.healthily.R
import com.example.healthily.Services.DataRecordingService
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    lateinit var toolBar: ActionBar
    private var selectedItemId = R.id.navigation_news
    private var lastItemId = 0

    override fun onPause() {
        super.onPause()
        val outState = Bundle()
        // Save the selected fragment for when the activity will come back foreground
        outState.putInt("selectedItemId", selectedItemId)
        Data.TemporaryState.CurrentHistoryFragment.savedState = outState

        /*
         * Save the streams in persistent storage, since onPause is the only method
         * guaranteed to be called before an app is destroyed, in normal conditions.
         */
        Data.Running.saveGeoStream(this)
        Data.Steps.saveSteps(this)
    }

    override fun onResume() {
        super.onResume()
        // Restore the saved temporary and persistent state
        val savedState = Data.TemporaryState.CurrentHistoryFragment.savedState
        if (savedState != null) {
            selectedItemId = savedState.getInt("selectedItemId")
            selectFragment(selectedItemId)
            Data.TemporaryState.CurrentHistoryFragment.savedState = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()

        Data.Running.loadGeoStream(this)
        Data.Steps.loadSteps(this)

        toolBar = supportActionBar!!

        val dataRecordingServiceClass = DataRecordingService::class.java

        // Start the fitness data recording service
        val stepServiceIntent = Intent(applicationContext, dataRecordingServiceClass)
        startService(stepServiceIntent)

        // Switch between fragments when the uset taps an item in the bottom navigation menu
        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        toolBar.title = getString(R.string.menu_news)
        val songsFragment = NewsFragment()
        openFragment(songsFragment)
    }

    /**
     * The event to be executed when the user taps on an item of the bottom navigation menu.
     */
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            selectFragment(item.itemId)
        }

    /**
     * Select one of the fragments of this activity given its id.
     */
    private fun selectFragment(item: Int): Boolean {
        when (item) {
            R.id.navigation_news -> {
                if (lastItemId != item) {
                    lastItemId = item
                    selectedItemId = item
                    toolBar.title = getString(R.string.menu_news)
                    val songsFragment = NewsFragment()
                    openFragment(songsFragment)
                    if (bottom_navigation.selectedItemId != R.id.navigation_news) {
                        bottom_navigation.selectedItemId = R.id.navigation_news
                    }
                }
                return true
            }
            R.id.navigation_steps -> {
                if (lastItemId != item) {
                    lastItemId = item
                    selectedItemId = item
                    toolBar.title = getString(R.string.menu_steps)
                    val songsFragment = FitnessFragment.newInstance(false)
                    openFragment(songsFragment)
                    if (bottom_navigation.selectedItemId != R.id.navigation_steps) {
                        bottom_navigation.selectedItemId = R.id.navigation_steps
                    }
                }
                return true
            }
            R.id.navigation_fitness -> {
                if (lastItemId != item) {
                    lastItemId = item
                    selectedItemId = item
                    toolBar.title = getString(R.string.menu_fitness)
                    val songsFragment = FitnessFragment.newInstance(true)
                    openFragment(songsFragment)
                    if (bottom_navigation.selectedItemId != R.id.navigation_fitness) {
                        bottom_navigation.selectedItemId = R.id.navigation_fitness
                    }
                }
                return true
            }
        }
        return false
    }

    /**
     * Open the given fragment in this activity.
     */
    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    /**
     * Ask for the location permission if necessary.
     */
    private fun checkPermissions() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            }
        } else {
            // Permission has already been granted
        }
    }

}

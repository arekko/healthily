package com.example.healthily.Controllers


import android.content.Context
import android.location.Location
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.healthily.Models.Data
import com.example.healthily.R
import com.example.healthily.Utils.GeoStream
import kotlinx.android.synthetic.main.activity_tracker.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

/**
 * A fragment presenting recorded running history on a map.
 */
class MapFragment : Fragment() {

    private lateinit var rotationGestureOverlay: RotationGestureOverlay
    private lateinit var compassOverlay: CompassOverlay
    private lateinit var trackOverlay: TrackOverlay

    /**
     * The start and the end timestamp of the data to be shown in the map.
     */
    private var startT: Double? = null
    private var endT: Double? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Retrieve the arguments of this fragment
        arguments?.getDouble("startT")?.let {
            startT = it
        }
        arguments?.getDouble("endT")?.let {
            endT = it
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Create the view, the map and all the overlays
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        Configuration.getInstance().load(context,
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(context))

        val mapView = view.map_detail
        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.ALWAYS)
        mapView.setMultiTouchControls(true)

        // The track overlay will draw the track on the map
        this.trackOverlay = TrackOverlay(startT, endT, false)
        mapView.overlays.add(this.trackOverlay)
        this.trackOverlay.updateLaps()

        rotationGestureOverlay = RotationGestureOverlay(mapView)
        rotationGestureOverlay.isEnabled = true

        this.compassOverlay =
            CompassOverlay(context, InternalCompassOrientationProvider(context), mapView)
        this.compassOverlay.enableCompass()
        mapView.overlays.add(this.compassOverlay)

        mapView.setTileSource(TileSourceFactory.MAPNIK)

        // The first position will be the first sample of the path that will be shown on the map
        val firstPosition = Data.Running.geoStream?.data?.firstAfter(startT ?: 0.0)

        if (firstPosition != null) {
            mapView.controller.setZoom(16.0)
            mapView.controller.setCenter(
                GeoPoint(firstPosition.latitude, firstPosition.longitude))
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(startT: Double, endT: Double) = MapFragment().apply {
            arguments = Bundle().apply {
                putDouble("startT", startT)
                putDouble("endT", endT)
            }
        }
    }

}

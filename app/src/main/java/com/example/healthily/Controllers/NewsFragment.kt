package com.example.healthily.Controllers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.healthily.Adapters.NewsRecyclerAdapter
import com.example.healthily.Models.NewsModel
import com.example.healthily.R
import com.example.healthily.Services.NewsService
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.android.synthetic.main.fragment_news.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NewsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_news, container, false)
        view.newsRecylerView.layoutManager = LinearLayoutManager(context)
        view.newsRecylerView.visibility = View.GONE
        view.progressBar.visibility = View.GONE

        view.swipeRefreshLayout.setOnRefreshListener {
            refreshItems(view)
        }
        callNewsService(view)
        return view
    }

    fun refreshItems(view: View) {
        view.newsRecylerView.visibility = View.GONE
        callNewsService(view)
    }

    fun onItemLoadComplete() {
        try {
            swipeRefreshLayout.isRefreshing = false
        } catch (ex: Exception) {

        }
    }

    fun callNewsService(view: View) {
        view.progressBar?.visibility = View.VISIBLE
        val call = NewsService.service.getHeadlines("us", "sports", 35, 1)
        val value = object : Callback<NewsModel> {
            override fun onResponse(
                call: Call<NewsModel>,
                response: Response<NewsModel>?
            ) {
                if (response != null) {
                    view.progressBar.visibility = View.GONE
                    val res: NewsModel = response.body()!!
                    view.newsRecylerView.visibility = View.VISIBLE
                    view.newsRecylerView.adapter =
                        NewsRecyclerAdapter(context!!, res.articles)
                    onItemLoadComplete()
                }
            }

            override fun onFailure(call: Call<NewsModel>, t: Throwable) {
                Log.e("DBG", t.toString())
            }
        }
        call.enqueue(value) // asynchronous request
    }

}

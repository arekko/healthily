package com.example.healthily.Controllers

import android.graphics.*
import android.view.MotionEvent
import com.example.healthily.Models.Data
import com.example.healthily.Utils.ValueStream
import com.example.healthily.Utils.ValueStreamIterator
import org.osmdroid.api.IGeoPoint
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Overlay
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/**
 * An overlay to draw a running track on a map.
 */
class TrackOverlay(
    // First timestamp of the data to show, if the map is not live (otherwise null)
    fromT: Double? = null,
    // Last timestamp of the data to show, if the map is not live (otherwise null)
    toT: Double? = null,
    // Allow the user to place markers by tapping on the map
    private val markersEnabled: Boolean = true): Overlay() {

    /**
     * A cache of the laps in the track.
     */
    private val laps = mutableListOf<Pair<String, GeoPoint>>()

    private val startT = fromT ?: Data.Running.currentTrackStartT
    private val endT = toT ?: Double.MAX_VALUE

    override fun draw(canvas: Canvas?, mapView: MapView?, shadow: Boolean) {
        super.draw(canvas, mapView, shadow)
        val dt = 3000.0 // 3 seconds between different samples
        val trackIterator =
            Data.Running.geoStream?.data?.iterator(startT, endT, dt, true)
                ?: ValueStreamIterator.empty()
        val speed = Data.Running.geoStream?.speed ?: ValueStream.empty()

        var startLineFrom: GeoPoint? = null

        // Draw lines between consecutive pairs of samples
        for (geoDataSample in trackIterator) {
            when (geoDataSample) {
                is ValueStreamIterator.Sample.None -> {
                    startLineFrom = null
                }
                is ValueStreamIterator.Sample.Value -> {
                    val geoData = geoDataSample.value
                    val latitude = geoData.latitude
                    val longitude = geoData.longitude

                    if (startLineFrom == null) {
                        startLineFrom = GeoPoint(latitude, longitude)
                    } else {
                        val endLineTo = GeoPoint(latitude, longitude)

                        // Draw the line
                        val runningSpeed = speed.valueAt(geoDataSample.time) ?: 0.0
                        drawSegment(canvas, mapView, startLineFrom, endLineTo, runningSpeed)

                        startLineFrom = endLineTo
                    }
                }
            }
        }

        drawLaps(canvas, mapView)

        if (markersEnabled) {
            drawMarkers(canvas, mapView)
        }
    }

    override fun onSingleTapConfirmed(e: MotionEvent?, mapView: MapView?): Boolean {
        super.onSingleTapConfirmed(e, mapView)
        if (!markersEnabled) {
            return true
        }
        // Place a marker if it doesn't exist, remove if it does.
        if (e != null && mapView != null) {
            val coords: IGeoPoint? = mapView.projection.fromPixels(e.x.toInt(), e.y.toInt())
            if (coords != null) {
                val geoPoint: GeoPoint = GeoPoint(coords.latitude, coords.longitude)
                val markers = Data.Markers.getAllMarkers()
                val pixelCoords = Point()
                mapView.projection.toPixels(geoPoint, pixelCoords)
                var removed = false // Has any marker been touched?
                for (marker in markers) {
                    val markerPixelPosition = Point()
                    mapView.projection.toPixels(marker, markerPixelPosition)
                    val dx = markerPixelPosition.x - pixelCoords.x
                    val dy = markerPixelPosition.y - pixelCoords.y
                    val distanceSqr = dx * dx + dy * dy
                    val tapDistance = 64
                    if (distanceSqr < tapDistance * tapDistance) {
                        Data.Markers.removeMarker(marker)
                        removed = true
                        break
                    }
                }
                if (!removed) {
                    Data.Markers.addMarker(geoPoint)
                }
            }
        }
        return true
    }

    /**
     * Update the lap cache, retrieving all the laps from the current track (identified by starT
     * and endT).
     */
    fun updateLaps() {
        laps.clear()
        val trackIterator =
            Data.Running.geoStream?.data?.iterator() ?: ValueStreamIterator.empty()
        var prevLap: String? = null
        var prevTrack: Int? = null
        for (geoDataSample in trackIterator) {
            if (geoDataSample is ValueStreamIterator.Sample.Value &&
                geoDataSample.time >= startT && geoDataSample.time <= endT) {
                // If the lap or the track has changed, register a new lap
                if (prevLap == null || prevLap != geoDataSample.value.tag ||
                    prevTrack == null || prevTrack != geoDataSample.value.trackIndex) {
                    laps.add(Pair(
                        geoDataSample.value.tag,
                        GeoPoint(geoDataSample.value.latitude, geoDataSample.value.longitude))
                    )
                    prevLap = geoDataSample.value.tag
                    prevTrack = geoDataSample.value.trackIndex
                }
            }
            if (geoDataSample is ValueStreamIterator.Sample.Value && geoDataSample.time > endT) {
                break
            }
        }
    }

    /**
     * Draw all the cached laps on the map.
     */
    private fun drawLaps(canvas: Canvas?, mapView: MapView?) {
        for ((lapName, lapCoords) in laps) {
            val color = Color.valueOf(0.8f, 0.2f, 0.8f).toArgb()
            drawText(canvas, mapView, lapName, lapCoords, color, shiftX = -24, shiftY = -36)
            drawPoint(canvas, mapView, lapCoords, color, pxRadius = 24.0f)
        }
    }

    /**
     * Draw all the markers created by the user.
     */
    private fun drawMarkers(canvas: Canvas?, mapView: MapView?) {
        val markers = Data.Markers.getAllMarkers()
        for (markerCoord in markers) {
            val color = Color.valueOf(0.9f, 0.4f, 0.1f).toArgb()
            drawText(canvas, mapView, "X", markerCoord, color, shiftX = -24, shiftY = -36)
            drawPoint(canvas, mapView, markerCoord, color, pxRadius = 24.0f)
        }
    }

    /**
     * Draw a point on the map, with the specified size (radius).
     */
    private fun drawPoint(canvas: Canvas?, mapView: MapView?, coords: GeoPoint, color: Int, pxRadius: Float = 1.0f) {
        val paint = Paint()
        paint.setDither(true)
        paint.setColor(color)
        paint.setStyle(Paint.Style.FILL_AND_STROKE)
        paint.setStrokeJoin(Paint.Join.ROUND)
        paint.setStrokeCap(Paint.Cap.ROUND)

        val position = Point()
        val projection = mapView?.projection
        projection?.toPixels(coords, position)

        canvas?.drawCircle(position.x.toFloat(), position.y.toFloat(), pxRadius, paint)
    }

    /**
     * Draw text on the map.
     */
    private fun drawText(canvas: Canvas?, mapView: MapView?, text: String, coords: GeoPoint,
                         color: Int, shiftX: Int = 0, shiftY: Int = 0) {
        val paint = Paint()
        paint.setDither(true)
        paint.setColor(color)
        paint.setStyle(Paint.Style.FILL_AND_STROKE)
        paint.setStrokeJoin(Paint.Join.ROUND)
        paint.setStrokeCap(Paint.Cap.ROUND)
        paint.setStrokeWidth(8.0f)
        paint.textSize = 120.0f

        val position = Point()
        val projection = mapView?.projection
        projection?.toPixels(coords, position)

        canvas?.drawText(text, (position.x + shiftX).toFloat(), (position.y + shiftY).toFloat(), paint)
    }

    /**
     * Draw a segment on the map, connecting two GeoPoint.
     */
    private fun drawSegment(canvas: Canvas?, mapView: MapView?,
                            startLineFrom: GeoPoint, endLineTo: GeoPoint, speed: Double) {
        // The saturated running speed will be 5 m/s (18 km/h)
        val maxRunningSpeed = 5.0
        val paint = Paint()
        paint.setDither(true)
        paint.setColor(
            Color.valueOf(
                min( abs(speed) / maxRunningSpeed, 1.0).toFloat(),
                max(1.0 - abs(speed) / maxRunningSpeed, 0.0).toFloat(),
                0.0f
            ).toArgb()
        )

        paint.setStyle(Paint.Style.FILL_AND_STROKE)
        paint.setStrokeJoin(Paint.Join.ROUND)
        paint.setStrokeCap(Paint.Cap.ROUND)
        paint.setStrokeWidth(max(12.0, 24.0*(1.0 - abs(speed) / maxRunningSpeed)).toFloat())

        val p1 = Point()
        val p2 = Point()
        val path = Path()

        val projection = mapView?.projection
        projection?.toPixels(startLineFrom, p1)
        projection?.toPixels(endLineTo, p2)

        path.moveTo(p2.x.toFloat(), p2.y.toFloat())
        path.lineTo(p1.x.toFloat(), p1.y.toFloat())

        canvas?.drawPath(path, paint)
    }
}

package com.example.healthily.Controllers

import android.location.Location
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.example.healthily.Models.Data
import com.example.healthily.R
import com.example.healthily.Utils.GeoStream
import kotlinx.android.synthetic.main.activity_tracker.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay


const val TAG = "DBG"

/**
 * Activity to record a running track while showing it on a map.
 */
class TrackerActivity : AppCompatActivity() {

    private lateinit var locationOverlay: MyLocationNewOverlay
    private lateinit var rotationGestureOverlay: RotationGestureOverlay
    private lateinit var compassOverlay: CompassOverlay
    private lateinit var trackOverlay: TrackOverlay
    private var firstLapInitialized = false

    /**
     * Called when a new location has been retrieved from the GPS sensor.
     */
    fun onLocationChanged(p0: Location?) {
        // Set the map to the current position
        mapView.controller.setCenter(GeoPoint(p0!!.latitude, p0.longitude))
    }

    /**
     * Called when a new location has been retrieved from the GPS sensor and the
     * recording into a stream is active.
     */
    fun onRecordingUpdate(stream: GeoStream) {
        val lastGeoData = stream.data.lastValue

        if (lastGeoData != null) {
            // Check for laps and markers
            val point = GeoPoint(lastGeoData.latitude, lastGeoData.longitude)

            // Initialize the first lap
            if (!firstLapInitialized) {
                firstLapInitialized = true
                trackOverlay.updateLaps()
            }

            if (Data.Markers.touchingMarker(point)) {
                Data.Running.newLap()
                trackOverlay.updateLaps()
                Data.Markers.removeMarker(point)
            }
        }
    }

    override fun onPause() {
        super.onPause()

        /*
         * Save the stream to persistent storage when the activity is not in foreground anymore and
         * could be destroyed.
         */
        Data.Running.saveGeoStream(this)
        Data.Steps.saveSteps(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * Create the map for the activity and set its overlays.
         */

        val ctx = applicationContext
        Configuration.getInstance().load(ctx,
            androidx.preference.PreferenceManager.getDefaultSharedPreferences(ctx))

        setContentView(com.example.healthily.R.layout.activity_tracker)
        mapView.setTileSource(TileSourceFactory.MAPNIK)
        mapView.controller.setZoom(16.0)

        mapView.zoomController.setVisibility(CustomZoomButtonsController.Visibility.ALWAYS)
        mapView.setMultiTouchControls(true)

        // Overlay the current location
        this.locationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(ctx), mapView)
        this.locationOverlay.enableMyLocation()
        mapView.overlays.add(locationOverlay)

        // Overlay the recorded track
        this.trackOverlay = TrackOverlay()
        mapView.overlays.add(this.trackOverlay)

        rotationGestureOverlay = RotationGestureOverlay(mapView)
        rotationGestureOverlay.isEnabled = true
        mapView.setMultiTouchControls(true)
        mapView.overlays.add(this.rotationGestureOverlay)

        this.compassOverlay =
            CompassOverlay(this, InternalCompassOrientationProvider(this), mapView)
        this.compassOverlay.enableCompass()
        mapView.overlays.add(this.compassOverlay)

        // Regiter as observer to receive the data updates for geolocation
        Data.Running.registerLocationObserver (this::onLocationChanged)
        Data.Running.registerRecordingObserver (this::onRecordingUpdate)

        // Set the first location to show on the map
        if (Data.Running.currentLocation != null) {
            mapView.controller.setCenter(
                GeoPoint(Data.Running.currentLocation!!.latitude,
                         Data.Running.currentLocation!!.longitude))
        }

        /**
         * Register the actions for the three buttons (Start/Stop, Lap and Done).
         */
        start_pause_fab.setOnClickListener {
            if (Data.Running.recording) {
                Data.Running.pauseRecording()
                Data.Running.pauseGeoStream()
                start_pause_fab.setImageResource(R.drawable.ic_play)
            } else {
                Data.Running.startRecording()
                start_pause_fab.setImageResource(R.drawable.ic_pause)
            }
        }

        lap_fab.setOnClickListener {
            Data.Running.newLap()
            trackOverlay.updateLaps()
        }

        done_fab.setOnClickListener {
            Data.Running.trackDone()
            Data.Running.pauseGeoStream()
            Data.Markers.clearAllMarkers()
            start_pause_fab.setImageResource(R.drawable.ic_play)
            onBackPressed()
        }

    }
}
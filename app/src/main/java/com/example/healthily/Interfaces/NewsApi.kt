package com.example.healthily.Interfaces

import com.example.healthily.Models.NewsModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

const val API_KEY = "2cda9e9005464b6987b4799fe7311336"

interface NewsApi {
    @Headers("X-Api-Key: ${API_KEY} ")
    @GET("top-headlines")
    fun getHeadlines(
        @Query("country") country: String,
        @Query("category") category: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int
    ): Call<NewsModel>
}
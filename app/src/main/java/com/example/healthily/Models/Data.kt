package com.example.healthily.Models

import android.content.Context
import android.location.Location
import android.os.Bundle
import com.example.healthily.Controllers.FitnessFragment
import com.example.healthily.Controllers.NESTING_DEPTH
import com.example.healthily.Utils.GeoStream
import com.example.healthily.Utils.MathUtils
import com.example.healthily.Utils.ValueStream
import org.osmdroid.util.GeoPoint
import java.io.*
import java.util.*

object Data {

    /**
     * Contains the state representing markers created by the user on the recording map.
     * Markers are stored in a separate object because it would be sensible to make them
     * persistent.
     */
    object Markers {

        private val lapMarkers: MutableList<GeoPoint> = mutableListOf()

        @Synchronized
        fun addMarker(point: GeoPoint) {
            if (!touchingMarker(point)) {
                lapMarkers.add(point)
            }
        }

        @Synchronized
        fun removeMarker(point: GeoPoint) {
            lapMarkers.firstOrNull { markersTouch(it, point) }?.let { lapMarkers.remove(it) }
        }

        @Synchronized
        fun clearAllMarkers() {
            lapMarkers.clear()
        }

        @Synchronized
        fun touchingMarker(point: GeoPoint): Boolean {
            return lapMarkers.any { markersTouch(it, point) }
        }

        @Synchronized
        fun getAllMarkers(): List<GeoPoint> {
            return lapMarkers
        }

        private fun markersTouch(pointA: GeoPoint, pointB: GeoPoint): Boolean {
            val touchDistance = 15.0 // meters
            val distance = MathUtils.haversineDistance(
                pointA.latitude, pointA.longitude,
                pointB.latitude, pointB.longitude)

            if (distance <= touchDistance) {
                return true
            }
            return false
        }
    }

    /**
     * Represents all the running data ever recorded by the app.
     * Allows to retrieve it, load it, save it, start and terminate tracks and laps,
     * start and stop the recording of geodata.
     */
    object Running {

        /**
         * The file where the data is going to be stored persistently.
         */
        private val geoStreamFile = "geoStream.str"

        @Volatile var currentLocation: Location? = null
        @Volatile var geoStream: GeoStream? = null
            private set
        @Volatile var currentTrack: Int = 0
            private set
        @Volatile var currentTrackStartT: Double = 0.0
            private set
        @Volatile var recordingObserver: (GeoStream) -> Unit = {}
            private set
        @Volatile var locationObserver: (Location) -> Unit = {}
            private set
        @Volatile var currentLap: Int = 1
            private set
        @Volatile var recording: Boolean = false
            private set

        @Synchronized
        fun updateGeoStream(time: Double, latitude: Double, longitude: Double, gpsSpeed: Double,
                            lap: String) {
            geoStream = geoStream?.withSampleAt(time, latitude, longitude, gpsSpeed, currentTrack, lap)
        }

        @Synchronized
        fun registerRecordingObserver(observer: (GeoStream) -> Unit) {
            this.recordingObserver = observer
        }

        @Synchronized
        fun registerLocationObserver(observer: (Location) -> Unit) {
            this.locationObserver = observer
        }

        @Synchronized
        fun newLap() {
            currentLap += 1

            val currentPosition: GeoStream.GeoData? = Data.Running.geoStream?.data?.lastValue
            val currentTime: Double? = Data.Running.geoStream?.data?.lastT
            val currentGpsSpeed: Double? = Data.Running.geoStream?.lastGpsSpeed

            if (currentPosition != null && currentTime != null && currentGpsSpeed != null) {
                updateGeoStream(currentTime, currentPosition.latitude, currentPosition.longitude,
                    currentGpsSpeed, currentLap.toString())

            }
        }

        @Synchronized
        fun pauseGeoStream() {
            geoStream = geoStream?.stop()
        }

        @Synchronized
        fun trackDone() {
            pauseRecording()
            currentLap = 1
            currentTrack += 1
            currentTrackStartT = (geoStream?.data?.lastT ?: 0.0)
        }

        @Synchronized
        fun pauseRecording() {
            recording = false
        }

        @Synchronized
        fun startRecording() {
            recording = true
        }

        @Synchronized
        fun loadGeoStream(context: Context) {
            if (geoStream == null) {
                try {
                    val file = File(context.getFilesDir(), geoStreamFile)
                    if (file.exists()) {
                        val objectInputStream =
                            ObjectInputStream(FileInputStream(file))
                        geoStream = objectInputStream.readObject() as? GeoStream

                        // The file contained a null or invalid geoStream
                        if (geoStream == null) {
                            geoStream = GeoStream()
                        }
                    } else {
                        geoStream = GeoStream()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }
            }
            val loadedTrack = geoStream?.data?.lastValue?.trackIndex ?: 0
            if (loadedTrack > currentTrack) {
                // When a track is loaded, start a new one after
                currentTrack = geoStream?.data?.lastValue?.trackIndex ?: 0 + 1
                trackDone()
            }
            // Start the new track right after the end of the last one
            currentTrackStartT = (geoStream?.data?.lastT ?: -0.01) + 0.01
        }

        @Synchronized
        fun saveGeoStream(context: Context) {
            if (geoStream != null && geoStream?.data?.isEmpty == false) {
                val objectOutputStream =
                    ObjectOutputStream(FileOutputStream(File(context.getFilesDir(), geoStreamFile)))
                objectOutputStream.writeObject(geoStream)
                objectOutputStream.close()
            }
        }
    }

    /**
     * Represents all the steps ever recorded by the app.
     */
    object Steps {

        private val stepStreamFile = "stepStream.str"

        @Volatile var stepStream: ValueStream<Double> =
            ValueStream.empty<Double>()
            @Synchronized private set

        @Synchronized
        fun addSteps(time: Double, steps: Double) {
            stepStream = stepStream
                    .withSampleAt(time, (stepStream.lastValue ?: 0.0) + steps)
        }

        @Synchronized
        fun loadSteps(context: Context) {
            if (stepStream.isEmpty) {
                try {
                    val file = File(context.getFilesDir(), stepStreamFile)
                    if (file.exists()) {
                        val objectInputStream =
                            ObjectInputStream(FileInputStream(file))
                        stepStream = (objectInputStream.readObject() as? ValueStream<Double>) ?: ValueStream.empty()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: ClassNotFoundException) {
                    e.printStackTrace()
                }
            }
        }

        @Synchronized
        fun saveSteps(context: Context) {
            if (!stepStream.isEmpty) {
                val objectOutputStream = ObjectOutputStream(
                    FileOutputStream(
                        File(
                            context.getFilesDir(),
                            stepStreamFile
                        )
                    )
                )
                objectOutputStream.writeObject(stepStream)
                objectOutputStream.close()
            }
        }
    }

    /**
     * Represents complex state that needs to be preserved when a fragment/activity gets
     * paused and the restored.
     */
    object TemporaryState {

        object CurrentHistoryFragment {

            @Volatile var savedState: Bundle? = null
                @Synchronized set
                @Synchronized get

        }

        object CurrentHistoryItem {

            @Volatile var savedState: Boolean = false
                @Synchronized set
                @Synchronized get

            @Volatile var historyStack = Stack<History>()
                @Synchronized set
                @Synchronized get

            @Volatile var depthCounter: Int = 0
                @Synchronized set
                @Synchronized get

            @Volatile var maxDepth: Int = 0
                @Synchronized set
                @Synchronized get

            @Volatile var historyType: FitnessFragment.HistoryType = FitnessFragment.HistoryType.RUNNING
                @Synchronized set
                @Synchronized get
        }

    }

}
package com.example.healthily.Models

/**
 * An item representing recorded sensor data history.
 */
class History(
    /**
     * The title of the history item.
     */
    val title: String,

    /**
     * The sum/average/other of an interesting quantity in the history.
     */
    val more: String,

    /**
     * The starting timestamp if the history.
     */
    val startT: Double,

    /**
     * The ending timestamp of the history.
     */
    val endT: Double
)
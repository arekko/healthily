package com.example.healthily.Models

/**
 * The result of a news query.
 */
class NewsModel(val status: String, val totalResults: Int, val articles: List<Articles>)

/**
 * An article of news.
 */
class Articles(
    val source: Source,
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String,
    val content: String
)

/**
 * The source of a news.
 */
class Source(
    id: String?,
    name: String
)



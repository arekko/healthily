package com.example.healthily.Services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.example.healthily.Controllers.MainActivity
import com.example.healthily.Controllers.TAG
import com.example.healthily.Models.Data
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.toast

/**
 * Records running and steps data in the background, when requested.
 */
class DataRecordingService : Service(), SensorEventListener, LocationListener {
    private val CHANNEL_ID = "HealtilyChannel"
    lateinit var sensorManager: SensorManager
    private var sStepCounter: Sensor? = null

    /**
     * The previous steps retrieved, to compute the difference (only the steps between
     * two moments).
     */
    private var lastSteps: Int? = null

    /**
     * Count how many steps have been recorded, to group them.
     */
    private var accumulator: Int = 0

    /**
     * How often will the GPS produce data, at minimum.
     */
    private val minimumUpdateInterval = 3000L //ms

    /**
     * How much the GPS has to move to produce new data, at minimum.
     * Higher value helps reduce the noise.
     */
    private val minimumUpdateDistance = 6f //m

    lateinit var locationManager: LocationManager

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        // Empty
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        val stepGroupSize = 50
        if (p0 != null && p0.sensor == sStepCounter) {
            val steps = if (lastSteps == null) {
                0
            } else {
                p0.values[0].toInt() - lastSteps!!
            }

            lastSteps = p0.values[0].toInt()

            accumulator += steps

            if (accumulator > stepGroupSize) {
                // Record the steps only in groups of stepGroupSize, to save memory
                Data.Steps.addSteps(System.currentTimeMillis().toDouble(), accumulator.toDouble())
                accumulator = 0
            }
        }
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        // Create the notification channel required by android to keep the service alive
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    override fun onCreate() {
        super.onCreate()

        /**
         * Initialize the location and step sensors.
         */

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minimumUpdateInterval, minimumUpdateDistance, this)
        } catch (e: SecurityException) {
            Log.d(TAG, e.toString())
        }

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        sStepCounter = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)

        if (sStepCounter != null) {
            sensorManager.registerListener(this, sStepCounter, SensorManager.SENSOR_DELAY_GAME)
            if (Data.Steps.stepStream.isEmpty) {
                Data.Steps.stepStream.withSampleAt(System.currentTimeMillis().toDouble(), 0.0)
            }
        } else {
            Toast.makeText(this, "No Step Counter Sensor!", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // Create the notification required by android to keep the service alive
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Healthily is running")
            .setContentText("")
            .setSmallIcon(android.R.drawable.ic_lock_lock)
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)

        return super.onStartCommand(intent, flags, startId)
    }

    @Synchronized
    override fun onLocationChanged(p0: Location?) {
        // Record the location and call the relevant callbacks on the UI thread
        if (p0 != null) {
            if (Data.Running.recording) {
                Data.Running.updateGeoStream(
                    System.currentTimeMillis().toDouble(), p0.latitude, p0.longitude,
                    p0.speed.toDouble(), Data.Running.currentLap.toString()
                )
                runOnUiThread { Data.Running.recordingObserver(Data.Running.geoStream!!) }
            }
            Data.Running.currentLocation = p0
            runOnUiThread { Data.Running.locationObserver(p0) }
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
        // Empty
    }

    override fun onProviderEnabled(p0: String?) {
        // Empty
    }

    override fun onProviderDisabled(p0: String?) {
        // Pause the stream if the GPS sensor is disabled
        Data.Running.pauseGeoStream()
    }

}

package com.example.healthily.Services

import com.example.healthily.Interfaces.NewsApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NewsService {
    const val URL = "https://newsapi.org/v2/"

    private val retrofit = Retrofit.Builder().baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val service = retrofit.create(NewsApi::class.java)
}


package com.example.healthily.Utils

import org.jetbrains.anko.coroutines.experimental.asReference
import java.io.Serializable

/**
 * Represents a stream of geolocation data.
 */
class GeoStream private constructor(
    val data: ValueStream<GeoData>): Serializable{

    constructor() : this(ValueStream.empty())

    /**
     * The total length of the path in the stream.
     */
    val totalDistance: Double by lazy {
        return@lazy distance.lastValue ?: 0.0
    }

    /**
     * The distance stream derived from this GeoStream.
     */
    val distance: ValueStream<Double> by lazy {
        data.foldDifferential<ValueStream<Double>, Double>(ValueStream.empty(), { first, second ->
            MathUtils.haversineDistance(first.latitude, first.longitude, second.latitude, second.longitude)
        }, {stream, current ->
            if (current != null) {
                stream.withSampleAt(current.t, (stream.lastValue ?: 0.0) + current.dvalue)
            } else {
                stream.stop()
            }
        })
    }

    /**
     *
     * The speed stream derived from this GeoStream.
     */
    val speed: ValueStream<Double> by lazy {
        return@lazy distance.derivative().map {it * 1000}
    }

    /**
     * The last recorded GPS speed, not computed from the distance data.
     */
    val lastGpsSpeed: Double? = data.lastValue?.gpsSpeed

    /**
     * Add a data sample at the specific time.
     */
    fun withSampleAt(time: Double, lat: Double, lon: Double, gpsSpeed: Double, trackIndex: Int,
                     tag: String = ""): GeoStream {
        return GeoStream(data.withSampleAt(time, GeoData(lat, lon, gpsSpeed, trackIndex, tag)))
    }

    /**
     * Add a data sample after a specific amount of time from the last one.
     */
    fun withSampleAfter(dt: Double, lat: Double, lon: Double, gpsSpeed: Double, trackIndex: Int,
                        tag: String = ""): GeoStream {
        return GeoStream(data.withSampleAfter(dt, GeoData(lat, lon, gpsSpeed, trackIndex, tag)))
    }

    /**
     * Stop the geostream continuity. Samples added later will not be continuous with samples
     * existing before the stop.
     */
    fun stop(): GeoStream {
        return GeoStream(data.stop())
    }

    /**
     * Represents a location sample made of a latitude and longitude in degrees,
     * a gps speed in m/s and an optional tag.
     */
    data class GeoData(val latitude: Double, val longitude: Double, val gpsSpeed: Double,
                       val trackIndex: Int, val tag: String): ValueStream.Interpolable, Serializable {
        override fun interpolate(other: ValueStream.Interpolable, factor: Double): ValueStream.Interpolable {
            if (other is GeoData) {
                return GeoData(latitude * (1.0 - factor) + other.latitude * factor,
                    longitude * (1.0 - factor) + other.longitude * factor,
                    gpsSpeed * (1.0 - factor) + other.gpsSpeed * factor, trackIndex, tag)
            } else {
                return this
            }
        }
    }
}
package com.example.healthily.Utils

object MathUtils {

    /**
     * Compute the distance in meters between two coordinates using the Haversine's formula.
     */
    fun haversineDistance(latA: Double, lonA: Double, latB: Double, lonB: Double): Double {

        fun degToRad(deg: Double): Double {
            return deg * Math.PI / 180
        }

        val R = 6371000
        val phiA = degToRad(latA)
        val phiB = degToRad(latB)
        val phiDiff = degToRad(latB-latA)
        val thetaDiff = degToRad(lonB-lonA)

        val a = Math.sin(phiDiff/2) * Math.sin(phiDiff/2) +
                Math.cos(phiA) * Math.cos(phiB) *
                Math.sin(thetaDiff/2) * Math.sin(thetaDiff/2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
        return R * c;
    }
}
package com.example.healthily.Utils

/**
 * Iterator for not nullable iterables that supports peek.
 */
class PeekIterator<T: Any> (private val iterator: Iterator<T>) : Iterator<T>{

    /**
     * The next item that will be returned by the iterator, stored so it can be peeked
     */
    private var nextT: T? = null

    override fun hasNext(): Boolean {
        if (nextT == null && iterator.hasNext()) {
            nextT = iterator.next()
        }
        return nextT != null
    }

    override fun next(): T {
        if (hasNext()) {
            val current = nextT
            if (iterator.hasNext()) {
                nextT = iterator.next()
            } else {
                nextT = null
            }
            if (current != null) {
                return current
            } else {
                // hasNext() guarantees that nextT (thus current) is not null
                throw InternalError()
            }
        } else {
            throw NoSuchElementException()
        }
    }

    /**
     * Returns the next item from the iterator but do not update the iterator.
     * If there is no next item, returns null.
     */
    fun peek(): T? {
        if (nextT == null && iterator.hasNext()) {
            nextT = iterator.next()
        }
        return nextT
    }

    /**
     * Returns the next item from the iterator and update the iterator.
     * If there is no next item, returns null.
     */
    fun nextOrNull(): T? {
        if (hasNext()) {
            return next()
        }
        return null
    }

}
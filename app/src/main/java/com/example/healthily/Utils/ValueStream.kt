package com.example.healthily.Utils

import java.io.Serializable
import java.lang.IllegalArgumentException
import kotlin.math.abs

/**
 * Represents a continuous sequence of values depending on an independent variable (called "t").
 */
open class ValueStream<T: Any> private constructor(
    private val samples: List<ValueStream.Sample<T>>, private val linearInterpolation: Boolean): Serializable {

    constructor(): this(listOf(), true) {}

    /**
     * True if this value stream does not contain data.
     */
    val isEmpty: Boolean = !samples.any { it is Sample.ValueSample }

    /**
     * True if this value stream is stopped, meaning that the last contiguous section of it has
     * been interrupted or that the stream is empty.
     */
    val isStopped: Boolean = isEmpty || samples.lastOrNull() is Sample.Stop

    /**
     * Get the value with the greatest t in the value stream.
     */
    val lastValue: T? = ((samples.lastOrNull { it is Sample.ValueSample }) as? Sample.ValueSample)?.value

    /**
     * Get the first t in this value stream.
     */
    val firstT: Double? = ((samples.firstOrNull { it is Sample.ValueSample }) as? Sample.ValueSample)?.t

    /**
     * Get the last t in this value stream
     */
    val lastT: Double? = ((samples.lastOrNull { it is Sample.ValueSample }) as? Sample.ValueSample)?.t

    /**
     * Small epsilon to decide when two samples are at the same t
     */
    private val EPSILON = Double.MIN_VALUE * 1000

    /**
     * Returns a copy of this value stream, but stopped. Adding a new sample to a stopped value stream
     * will resume it, creating a new separate section.
     */
    fun stop(): ValueStream<T> {
        if (!isStopped && !isEmpty) {
            return ValueStream(samples + Sample.Stop(), linearInterpolation)
        }
        return this
    }

    /**
     * Returns a copy of this value stream, but interpolated with a constant function.
     */
    fun withConstantInterpolation(): ValueStream<T> {
        return ValueStream(this.samples, false)
    }

    /**
     * Returns a copy of this value stream, but interpolated with a constant function.
     */
    fun withLinearInterpolation(): ValueStream<T> {
        return ValueStream(this.samples, true)
    }

    /**
     * Returns a value stream made of this value stream and a sample added at the specified t.
     */
    fun withSampleAt(t: Double, value: T): ValueStream<T> {
        return ValueStream(insertValueOrdered(samples, Sample.ValueSample(t, value)), linearInterpolation)
    }

    /**
     * Returns a value stream made of this value stream and a sample added at the end of
     * this value stream, after the specified positive t interval.
     */
    fun withSampleAfter(dt: Double, value: T): ValueStream<T> {
        if (dt < 0.0) {
            throw IllegalArgumentException("The time interval should be positive or zero.")
        } else if (dt < EPSILON) {
            // If the dt is close to zero, we are replacing the last sample
            val valueSamples = samples.dropLastWhile { it is Sample.Stop }
            val lastTime = ((valueSamples.lastOrNull()) as? Sample.ValueSample)?.t ?: 0.0
            return ValueStream(
                samples.dropLastWhile { it is Sample.Stop }.dropLast(1) +
                        Sample.ValueSample(lastTime, value), linearInterpolation)
        }
        // Add the new sample after the specified t
        val prevt =
            ((samples.lastOrNull { it is Sample.ValueSample }) as? Sample.ValueSample)?.t ?: 0.0
        return ValueStream(samples + Sample.ValueSample(prevt + dt, value), linearInterpolation)
    }

    /**
     * Merge two value streams.
     */
    operator fun plus(other: ValueStream<T>): ValueStream<T> {
        return ValueStream(mergeSamples(samples, other.samples), linearInterpolation)
    }

    /**
     * Returns the value of this stream at the specified t.
     */
    fun valueAt(t: Double): T? {
        // True if an element precede the specified t
        val precede = {it: Sample<T> ->
            when(it) {
                is Sample.ValueSample -> it.t < t
                else -> true
            }
        }

        // Find the preceding sample (< t), if it exists and its not a Stop
        val preceding = (samples
            .takeWhile(precede)
            .lastOrNull()) as? Sample.ValueSample

        // Find the following sample (>= t), if it exists and its not a Stop
        val following = (samples.find{!precede(it)}) as? Sample.ValueSample

        if (preceding == null && following != null && abs(following.t - t) < EPSILON) {
            // If there is no preceding sample and the following is at the requested position
            return following.value
        } else if (preceding == null || following == null) {
            return null
        }

        val doubleA = (preceding.value) as? Double
        val doubleB = (following.value) as? Double
        val interpA = (preceding.value) as? Interpolable
        val interpB = (following.value) as? Interpolable
        val isInterpolable = (doubleA != null && doubleB != null) || (interpA != null && interpB != null)

        // If we are not meant to interpolate or it is not possible
        if (!linearInterpolation || !isInterpolable) {
            if (abs(following.t - t) < EPSILON) {
                return following.value
            } else {
                return preceding.value
            }
        }

        // Linear interpolation between the samples in between which the requested t is located
        val dt = following.t - preceding.t
        val factor = if (abs(dt) <= EPSILON) {
            0.5
        } else {
            (t - preceding.t) / dt
        }

        if (doubleA != null && doubleB != null) {
            // This unchecked cast is safe, as T is Double in here
            @Suppress("UNCHECKED_CAST")
            return (doubleA * (1.0 - factor) + doubleB * factor) as? T?
        } else if (interpA != null && interpB != null) {
            // This unchecked cast is safe, as T is Interpolable in here
            @Suppress("UNCHECKED_CAST")
            return interpA.interpolate(interpB, factor) as? T?
        }

        // This should never be reached
        throw InternalError()
    }

    /**
     * Return the first available data after the specified t. If data is available at t, returns
     * the same as valueAt(t).
     */
    fun firstAfter(t: Double): T? {
        val valueAtT = valueAt(t)

        if (valueAtT != null) {
            return valueAtT
        }

        return (samples.firstOrNull { it is Sample.ValueSample && it.t >= t } as? Sample.ValueSample)?.value
    }

    /**
     * Return the first available data before the specified t. If data is available at t, returns
     * the same as valueAt(t).
     */
    fun firstBefore(t: Double): T? {
        val valueAtT = valueAt(t)

        if (valueAtT != null) {
            return valueAtT
        }

        return (samples.lastOrNull { it is Sample.ValueSample && it.t <= t } as? Sample.ValueSample)?.value
    }

    /**
     * Returns a subsection of this stream starting at startT and ending at endT.
     */
    fun section(startT: Double, endT: Double): ValueStream<T> {
        // Get the interpolated value ad the boundaries of the section
        val startValue = valueAt(startT)
        val endValue = valueAt(endT)

        // Make a value stream with all the samples in the specified interval
        val subStream = ValueStream<T>(
            samples
                .dropWhile {
                    when(it) {
                        is Sample.ValueSample -> startT - it.t > EPSILON
                        else -> true
                    }
                }
                .dropWhile { it is Sample.Stop }
                .takeWhile {
                    when(it) {
                        is Sample.ValueSample -> endT - it.t > -EPSILON
                        else -> true
                    }
                }, linearInterpolation)

        /*
         * If the values at the boundaries are defined, add them as first and/or last sample,
         * otherwise just return the in-between samples
         */
        if (startValue != null && endValue != null) {
            return subStream.withSampleAt(startT, startValue).withSampleAt(endT, endValue)
        } else if (startValue != null && endValue != null) {
            return subStream.withSampleAt(startT, startValue).withSampleAt(endT, endValue)
        } else if (startValue != null && endValue != null) {
            return subStream.withSampleAt(startT, startValue).withSampleAt(endT, endValue)
        } else {
            return subStream
        }

    }

    /**
     * Returns a list iterator to the values of samples of this stream, separated by arbitrary
     * values of t.
     */
    fun iterator(): Iterator<ValueStreamIterator.Sample<T>> {
        return object : Iterator<ValueStreamIterator.Sample<T>> {
            val iter = samples.iterator()

            override fun hasNext(): Boolean {
                return iter.hasNext()
            }

            override fun next(): ValueStreamIterator.Sample<T> {
                val elem = iter.next()

                if (elem is Sample.ValueSample) {
                    return ValueStreamIterator.Sample.Value(elem.t, elem.value)
                } else {
                    return ValueStreamIterator.Sample.None
                }
            }

        }
    }

    /**
     * Returns an iterator to samples of this value stream, from startT to endT.
     */
    fun iterator(startT: Double, endT: Double, dt: Double,
                 forceIncludeLast: Boolean = false): Iterator<ValueStreamIterator.Sample<T>> {
        return ValueStreamIterator(this, startT, endT, dt, forceIncludeLast)
    }

    /**
     * Returns an iterator to samples of this value stream.
     */
    fun iterator(dt: Double, forceIncludeLast: Boolean = false): Iterator<ValueStreamIterator.Sample<T>> {
        return ValueStreamIterator(this, dt, forceIncludeLast)
    }

    /**
     * Returns samples of this stream, separated by the specified t interval.
     */
    fun sample(dt: Double): List<T?> {
        val iterator = iterator(dt)
        val result: MutableList<T?> = mutableListOf()
        iterator.forEachRemaining {
            when(it) {
                is ValueStreamIterator.Sample.None -> result.add(null)
                is ValueStreamIterator.Sample.Value -> result.add(it.value)
            }
        }
        return result
    }

    /**
     * Fold the values in this value stream. The folding function right parameter is null if there
     * is no current sample because the stream is interrupted.
     */
    fun <U> foldAbsolutet(initial: U, foldingFunction: (U, AbsoluteFoldingParameter?) -> U): U {
        return samples.foldRight(initial){ sample, acc ->
            when(sample) {
                is Sample.ValueSample ->
                    foldingFunction(acc, AbsoluteFoldingParameter(sample.t, sample.value))
                else -> foldingFunction(acc, null)
            }

        }
    }

    /**
     * Fold the values in this value stream with t intervals. The folding function right parameter
     * is null if there is no current sample because the stream is interrupted.
     */
    fun <U> foldtInterval(initial: U, foldingFunction: (U, IntervalFoldingParameter?) -> U): U {
        var acc = initial
        for (i in samples.indices) {
            val prevSample = (samples.getOrNull(i - 1)) as? Sample.ValueSample
            val currentSample = (samples.getOrNull(i)) as? Sample.ValueSample
            val nextSample = (samples.getOrNull(i + 1)) as? Sample.ValueSample

            // The current sample must exist and be a valid value
            if (currentSample != null) {
                if (prevSample == null && nextSample == null) {
                    // We are looking at the only sample in the stream or section of a stream
                    acc = foldingFunction(acc,
                        IntervalFoldingParameter(currentSample.t, 0.0, currentSample.value))
                } else if (prevSample == null && nextSample != null) {
                    /*
                     * We are looking at the first sample of a section of the stream containing
                     * multiple samples
                     */
                    val dt = (nextSample.t - currentSample.t) / 2
                    acc = foldingFunction(acc,
                        IntervalFoldingParameter(currentSample.t, dt, currentSample.value))
                } else if (prevSample != null && nextSample == null) {
                    /*
                     * We are looking at the last sample of a section of the stream containing
                     * multiple samples
                     */
                    val dt = (currentSample.t - prevSample.t) / 2
                    acc = foldingFunction(acc,
                        IntervalFoldingParameter(currentSample.t, dt, currentSample.value))
                } else if (prevSample != null && nextSample != null) {
                    /*
                     * We are looking at a sample in a section of the stream, followed and preceded
                     * by other samples
                     */
                    val prevDt = (currentSample.t - prevSample.t) / 2
                    val nextDt = (nextSample.t - currentSample.t) / 2
                    acc = foldingFunction(acc,
                        IntervalFoldingParameter(
                            currentSample.t, prevDt + nextDt, currentSample.value))
                }
            } else {
                // The stream is interrupted in this point
                acc = foldingFunction(acc, null)
            }
        }
        return acc
    }

    /**
     * Fold the values in this value stream by providing t and value differential.
     * The folding function right parameter is null if there is no current sample because the stream
     * is interrupted.
     */
    fun <U, D> foldDifferential(initial: U, difference: (T, T) -> D,
                             foldingFunction: (U, DifferentialFoldingParameter<D>?) -> U): U {
        var acc = initial

        for (i in samples.indices) {
            val prevSample = samples.getOrNull(i - 1) as? Sample.ValueSample
            val currentSample = samples.getOrNull(i) as? Sample.ValueSample
            val nextSample = samples.getOrNull(i + 1) as? Sample.ValueSample

            // The current sample must exist and be a valid value
            if (currentSample != null) {
                if (prevSample == null && nextSample == null) {
                    /*
                     * We are looking at the only sample in the stream or section of a stream, so
                     * the differentials are undefined.
                     */
                } else if (prevSample == null && nextSample != null) {
                    /*
                     * We are looking at the first sample of a section of the stream containing
                     * multiple samples
                     */
                    val tRight = (nextSample.t + currentSample.t) / 2
                    val vRight = valueAt(tRight)!!
                    val dvalue = difference(vRight, currentSample.value)
                    val dt = tRight - currentSample.t
                    acc = foldingFunction(acc,
                        DifferentialFoldingParameter(currentSample.t, dt, dvalue))
                } else if (prevSample != null && nextSample == null) {
                    /*
                     * We are looking at the last sample of a section of the stream containing
                     * multiple samples
                     */
                    val tLeft = (currentSample.t + prevSample.t) / 2
                    val vLeft = valueAt(tLeft)!!
                    val dt = currentSample.t - tLeft
                    val dvalue = difference(currentSample.value, vLeft)
                    acc = foldingFunction(acc,
                        DifferentialFoldingParameter(currentSample.t, dt, dvalue))
                } else if (prevSample != null && nextSample != null) {
                    /*
                     * We are looking at a sample in a section of the stream, followed and preceded
                     * by other samples
                     */
                    val tLeft = (currentSample.t + prevSample.t) / 2
                    val tRight = (nextSample.t + currentSample.t) / 2
                    val vLeft = valueAt(tLeft)!!
                    val vRight = valueAt(tRight)!!
                    val dt = tRight - tLeft
                    val dvalue = difference(vRight, vLeft)
                    acc = foldingFunction(acc,
                        DifferentialFoldingParameter(
                            currentSample.t, dt, dvalue))
                }
            } else {
                // The stream is interrupted in this point
                acc = foldingFunction(acc, null)
            }
        }
        return acc
    }

    /**
     * Returns a new value stream with the values of this value stream mapped using the specified
     * function.
     */
    fun <U: Any> map (mapping: (T) -> U): ValueStream<U> {
        return ValueStream<U>(samples.map{
            when (it) {
                is Sample.ValueSample -> Sample.ValueSample(it.t, mapping(it.value))
                is Sample.Stop -> Sample.Stop()
            }
        }, linearInterpolation)
    }

    /**
     * Merge the two given sample lists. The two sample lists are assumed to be ordered and
     * not overlap.
     * Throw an IllegalArgumentException if the streams cannot be merged because they
     * overlap.
     * Throw as InternalError if one of the streams is malformed.
     */
    private fun <T> mergeSamples(samples: List<Sample<T>>, others: List<Sample<T>>): List<Sample<T>> {
        val firstA = samples.firstOrNull { it is Sample.ValueSample } as? Sample.ValueSample
        val firstB = others.firstOrNull { it is Sample.ValueSample } as? Sample.ValueSample
        val lastA = samples.lastOrNull { it is Sample.ValueSample } as? Sample.ValueSample
        val lastB = others.lastOrNull { it is Sample.ValueSample } as? Sample.ValueSample

        val isEmptyA = firstA == null || lastA == null
        val isEmptyB = firstB == null || lastB == null

        if (isEmptyA && isEmptyB) {
            return listOf()
        } else if (isEmptyA) {
            return others
        } else if (isEmptyB) {
            return samples
        }

        val firstAt = firstA?.t ?: 0.0
        val firstBt = firstB?.t ?: 0.0
        val lastAt = lastA?.t ?: 0.0
        val lastBt = lastB?.t ?: 0.0

        val correct = (firstAt <= lastAt) && (firstBt < lastBt)
        val intersect = (firstAt < lastBt) && (lastAt > firstBt)

        if (!correct) {
            throw InternalError("One or both of the streams are malformed.")
        }

        if (intersect) {
            throw IllegalArgumentException("The streams cannot be merged as they intersect")
        }

        // Find out which stream comes first
        val isAFirst = lastAt <= firstBt
        val firstStream = if (isAFirst) samples else others
        val secondStream = if (isAFirst) others else samples
        val streamsTouch = if (isAFirst) {
            abs(lastAt - firstBt) < EPSILON
        } else {
            abs(lastBt - firstAt) < EPSILON
        }

        return if (streamsTouch) {
            /*
             * Replace the last value of the first stream with the first value of the second
             * and remove any stops in between
             */
            firstStream.dropLastWhile {it is Sample.Stop }.dropLast(1) +
                    secondStream.dropWhile { it is Sample.Stop }
        } else {
            firstStream + secondStream
        }
    }

    /**
     * Insert a value sample in the right place in the stream.
     */
    private fun <T> insertValueOrdered(samples: List<Sample<T>>, sample: Sample.ValueSample<T>): List<Sample<T>> {
        val at = sample.t
        // True if an element precede the specified t
        val precede = {it: Sample<T> ->
            when(it) {
                is Sample.ValueSample -> it.t <= at || abs(it.t - at) < EPSILON
                else -> true
            }
        }

        val firstPart = samples.takeWhile(precede)
        val secondPart = samples.dropWhile(precede)

        val prevT = (firstPart.lastOrNull() { it is Sample.ValueSample } as? Sample.ValueSample)?.t
        val stopsInBetween = firstPart.lastOrNull() is Sample.Stop

        /*
         * Remove the last sample of the first part if it has the same t as the sample we are
         * inserting.
         */
        val firstPartTrimmed = if (prevT != null && abs(at - prevT) < EPSILON) {
            if (stopsInBetween) {
                firstPart.dropLastWhile {it is Sample.Stop }.dropLast(1)
            } else {
                firstPart.dropLast(1)
            }
        } else {
            firstPart
        }

        val stopsTrimmed = stopsInBetween && firstPart.size != firstPartTrimmed.size

        if (stopsTrimmed) {
            // Add again the trimmed stop
            return firstPartTrimmed + sample + Sample.Stop() + secondPart
        } else {
            return firstPartTrimmed + sample + secondPart
        }
    }

    companion object {

        /**
         * An empty value stream.
         */
        fun <T: Any> empty(): ValueStream<T> {
            return ValueStream<T>(listOf(), true)
        }
    }

    /**
     * A discrete sample in the value stream.
     */
    private sealed class Sample<out T>(): Serializable {

        /**
         * Represents a stop in the value stream
         */
        class Stop : Sample<Nothing>(), Serializable

        /**
         * A discrete sample that holds a value
         */
        class ValueSample<T>(
            val t: Double,
            val value: T
        ) : Sample<T>(), Serializable
    }

    /**
     * Represents the current element of an absolute t fold over a value stream.
     */
    inner class AbsoluteFoldingParameter(val t: Double, val value: T)

    /**
     * Represents the current element of a t interval fold over a value stream.
     */
    inner class IntervalFoldingParameter(val t: Double, val dt: Double, val value: T)

    /**
     * Represents the current element of a differential t fold over a value stream.
     */
    inner class DifferentialFoldingParameter<D>(val t: Double, val dt: Double, val dvalue: D)

    /**
     * Represents a composite value that can be linearly interpolated.
     */
    interface Interpolable {

        /**
         * Interpolate a value with another value given a mixing factor from 0.0 to 1.0.
         */
        fun interpolate(other: Interpolable, factor: Double): Interpolable

    }

}

/**
 * Compute the t derivative of this stream.
 */
fun ValueStream<Double>.derivative(): ValueStream<Double> {
    return foldDifferential(ValueStream.empty(), Double::minus, { tStream, current ->
        if (current != null) {
            if (current.dt == 0.0) {
                return@foldDifferential tStream
            }
            return@foldDifferential tStream
                .withSampleAt(current.t, current.dvalue / current.dt)
        } else {
            return@foldDifferential tStream.stop()
        }
    })
}

/**
 * Compute the t integral of this stream.
 */
fun ValueStream<Double>.integral(): ValueStream<Double> {
    return foldtInterval(ValueStream.empty(), { tStream, current ->
        if (current != null) {
            val lastValue = tStream.lastValue ?: 0.0
            return@foldtInterval tStream
                .withSampleAt(current.t, lastValue + current.value * current.dt)
        } else {
            return@foldtInterval tStream.stop()
        }
    })
}
package com.example.healthily.Utils

import kotlin.math.max
import kotlin.math.min

/**
 * Iterator to iterate samples in a value stream, specifying a starting point, and
 * ending point and a sampling interval.
 */
class ValueStreamIterator<T: Any> constructor(
    private val source: ValueStream<T>,
    startT: Double,
    endT: Double,
    private val dt: Double,
    forceIncludeLast: Boolean = false): Iterator<ValueStreamIterator.Sample<T>> {

    /**
     * Create an iterator that iterates through the whole value stream.
     */
    constructor(source: ValueStream<T>, dt: Double, forceIncludeLast: Boolean = false) : this(
        source, source.firstT ?: 0.0, source.lastT ?: -1.0, dt, forceIncludeLast)

    private val firstT = max(source.firstT ?: 0.0, startT)
    private val lastT = min(source.lastT ?: -1.0, endT)
    private var currentT = firstT

    // Has the last sample been returned? Also set to true if we don't need to return the last sample.
    private var lastReturned = !forceIncludeLast

    override fun hasNext(): Boolean {
        return currentT <= lastT && !lastReturned
    }

    override fun next(): Sample<T> {
        if (!hasNext()) {
            throw NoSuchElementException()
        }

        if (currentT >= lastT && !lastReturned) {
            val lastValue = source.valueAt(lastT)
            currentT += dt
            lastReturned = true
            return if (lastValue != null) Sample.Value(lastT, lastValue) else Sample.None
        }

        val value = source.valueAt(currentT)
        val t = currentT
        currentT += dt
        return if (value != null) Sample.Value(t, value) else Sample.None
    }

    /**
     * A sample from the value stream, can be None or have a value.
     */
    sealed class Sample<out T: Any> {
        object None: Sample<Nothing>()
        data class Value<T: Any>(val time: Double, val value: T): Sample<T>()
    }

    companion object {
        fun <E: Any> empty() = ValueStreamIterator(ValueStream.empty<E>(), 0.0, -1.0, 0.0)
    }

}

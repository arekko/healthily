package com.example.healthily.Utils

import org.junit.Test

import org.junit.Assert.*

class GeoStreamTest {

    @Test
    fun getTotalDistanceTest() {
        val geoStream = GeoStream()
            .withSampleAfter(1.0, 60.197343, 24.902838, 1.0, 0)
            .withSampleAfter(1.0, 60.197357, 24.903611, 1.0, 0)
            .withSampleAfter(1.0, 60.197397, 24.905109, 1.0, 0)
            .withSampleAfter(1.0, 60.197424, 24.906042, 1.0, 0)
            .withSampleAfter(1.0, 60.197432, 24.906246, 1.0, 0)
            .withSampleAfter(1.0, 60.197440, 24.906761, 1.0, 0)
            .withSampleAfter(1.0, 60.197464, 24.907930, 1.0, 0)

        assertEquals(280.0, geoStream.totalDistance, 3.0)

    }
}
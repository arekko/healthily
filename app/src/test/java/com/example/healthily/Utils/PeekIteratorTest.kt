package com.example.healthily.Utils

import org.junit.Test

import org.junit.Assert.*

class PeekIteratorTest {

    val emptyList: List<Int> = listOf()
    val singletonList: List<Int> = listOf(1)
    val multiList: List<Int> = listOf(1, 2, 3, 4, 5)

    val emptyListIterator = PeekIterator(emptyList.listIterator())
    val singletonListIterator = PeekIterator(singletonList.listIterator())
    val multiListIterator = PeekIterator(multiList.listIterator())

    @Test
    fun hasNextTest() {
        assertFalse(emptyListIterator.hasNext())

        assertTrue(singletonListIterator.hasNext())
        assertEquals(1, singletonListIterator.next())
        assertFalse(singletonListIterator.hasNext())

        var count = 0
        while (multiListIterator.hasNext()) {
            assertEquals(count + 1, multiListIterator.next())
            count++
        }

        assertEquals(5, count)
        assertFalse(multiListIterator.hasNext())
    }

    @Test
    fun nextText() {
        assertEquals(1, singletonListIterator.next())

        var count = 0
        while (multiListIterator.hasNext()) {
            assertEquals(count + 1, multiListIterator.next())
            count++
        }

        /*
         * Check that NoSuchElementException is thrown when calling next on an empty/exhausted
         * iterator
         */

        var correctException = false
        try {
            multiListIterator.next()
        } catch (ex: NoSuchElementException) {
            correctException = true
        }
        assertTrue(correctException)

        correctException = false
        try {
            emptyListIterator.next()
        } catch (ex: NoSuchElementException) {
            correctException = true
        }
        assertTrue(correctException)
    }

    @Test
    fun peekTest() {

        assertEquals(null, emptyListIterator.peek())

        assertEquals(1, singletonListIterator.peek())
        assertEquals(1, singletonListIterator.peek())
        assertEquals(1, singletonListIterator.next())
        assertEquals(null, singletonListIterator.peek())

        assertEquals(1, multiListIterator.peek())
        assertEquals(1, multiListIterator.next())
        assertEquals(2, multiListIterator.peek())
    }

    @Test
    fun nextOrNullTest() {
        assertEquals(null, emptyListIterator.nextOrNull())

        assertEquals(1, singletonListIterator.nextOrNull())
        assertEquals(null, singletonListIterator.nextOrNull())

        var count = 0
        while (multiListIterator.hasNext()) {
            assertEquals(count + 1, multiListIterator.nextOrNull())
            count++
        }
        assertEquals(5, count)
        assertEquals(null, multiListIterator.nextOrNull())
    }
}
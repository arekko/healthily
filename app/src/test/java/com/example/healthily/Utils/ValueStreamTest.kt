package com.example.healthily.Utils

import org.junit.Test

import org.junit.Assert.*
import java.lang.IllegalArgumentException

class ValueStreamTest {

    val DOUBLE_DELTA = 0.001

    val emptyValueStream = ValueStream.empty<Double>()

    @Test
    fun isEmptyTest() {

        // Test that the stream returns empty when it should
        assertTrue(emptyValueStream.isEmpty)
        assertFalse(emptyValueStream.withSampleAfter(1.0, 1.0).isEmpty)
        assertFalse(emptyValueStream.withSampleAt(1.0, 1.0).isEmpty)
        assertFalse(emptyValueStream.withSampleAt(0.0, 1.0)
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(2.0, 5.0)
            .isEmpty)

        // Test that after creating streams out of an empty stream the original stays empty
        assertTrue(emptyValueStream.isEmpty)

        // Check that a stopped empty stream is still empty
        assertTrue(emptyValueStream.stop().isEmpty)
        assertTrue(emptyValueStream.stop().stop().isEmpty)

        // Check that a stopped non empty stream is still not empty
        assertFalse(emptyValueStream.withSampleAfter(1.0, 1.0).stop().isEmpty)
        assertFalse(emptyValueStream.stop().withSampleAfter(1.0, 1.0).stop().isEmpty)
        assertFalse(emptyValueStream.stop().withSampleAfter(1.0, 1.0).isEmpty)
    }

    @Test
    fun isStoppedTest() {

        // Test that the stream returns stopped when it should
        assertTrue(emptyValueStream.isStopped) // An empty stream is stopped
        assertFalse(emptyValueStream.withSampleAfter(1.0, 1.0).isStopped)
        assertFalse(emptyValueStream.withSampleAt(1.0, 1.0).isStopped)
        assertTrue(emptyValueStream.withSampleAt(1.0, 1.0).stop().isStopped)
        assertFalse(emptyValueStream.stop().withSampleAt(1.0, 1.0).isStopped)
        assertTrue(emptyValueStream.stop().withSampleAt(1.0, 1.0).stop().isStopped)
        assertFalse(emptyValueStream.withSampleAt(0.0, 1.0)
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(2.0, 5.0)
            .isStopped)

        // Test that after creating streams out of a stopped stream the original stays stopped
        assertTrue(emptyValueStream.isStopped)

        // Check that a stopped empty stream is still stopped
        assertTrue(emptyValueStream.stop().isStopped)
        assertTrue(emptyValueStream.stop().stop().isStopped)
    }

    @Test
    fun stopIntervalTest() {

        val firstPart = emptyValueStream
            .withSampleAfter(0.0, 1.0)
            .withSampleAfter(1.0, 2.0)

        val secondPart = emptyValueStream
            .withSampleAfter(3.0, 3.0)
            .withSampleAfter(1.0, 6.0)

        val stream = firstPart.stop() + secondPart

        assertEquals(null, stream.valueAt(Double.NEGATIVE_INFINITY))
        assertEquals(null, stream.valueAt(-Double.MAX_VALUE))
        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(1.0, stream.valueAt(0.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.3, stream.valueAt(0.3) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(1.1))
        assertEquals(null, stream.valueAt(1.5))
        assertEquals(null, stream.valueAt(2.0))
        assertEquals(null, stream.valueAt(2.7))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(3.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(6.1))
        assertEquals(null, stream.valueAt(10.0))
        assertEquals(null, stream.valueAt(Double.MAX_VALUE))
        assertEquals(null, stream.valueAt(Double.POSITIVE_INFINITY))
    }

    @Test
    fun withSampleAtTest() {
        val stream = emptyValueStream
            .withSampleAt(0.0, 1.0)
            .withSampleAt(1.0, 2.0)
            .withSampleAt(-1.0, 0.0)

        assertEquals(null, stream.valueAt(-2.0))
        assertEquals(0.0, stream.valueAt(-1.0) ?: Double.MAX_VALUE, DOUBLE_DELTA)
        assertEquals(0.5, stream.valueAt(-0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, stream.valueAt(0.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, stream.valueAt(0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(1.1))

        val streamWithStop = emptyValueStream
            .withSampleAt(0.0, 1.0)
            .withSampleAt(1.0, 2.0)
            .stop()
            .withSampleAt(2.0, 3.0)
            .withSampleAt(-1.0, 0.0)

        assertEquals(null, streamWithStop.valueAt(-1.1))
        assertEquals(0.0, streamWithStop.valueAt(-1.0) ?: Double.MAX_VALUE, DOUBLE_DELTA)
        assertEquals(0.5, streamWithStop.valueAt(-0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, streamWithStop.valueAt(0.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, streamWithStop.valueAt(0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, streamWithStop.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, streamWithStop.valueAt(1.01))
        assertEquals(null, streamWithStop.valueAt(1.5))
        assertEquals(null, streamWithStop.valueAt(1.99))
        assertEquals(3.0, streamWithStop.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, streamWithStop.valueAt(3.1))

        val streamWithStopAtInside = emptyValueStream
            .withSampleAt(0.0, 1.0)
            .withSampleAt(1.0, 2.0)
            .stop()
            .withSampleAt(2.0, 3.0)
            .withSampleAt(1.5, 2.5)

        assertEquals(null, streamWithStopAtInside.valueAt(-1.1))
        assertEquals(null, streamWithStopAtInside.valueAt(-1.0))
        assertEquals(null, streamWithStopAtInside.valueAt(-0.5))
        assertEquals(1.0, streamWithStopAtInside.valueAt(0.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, streamWithStopAtInside.valueAt(0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, streamWithStopAtInside.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, streamWithStopAtInside.valueAt(1.01))
        assertEquals(null, streamWithStopAtInside.valueAt(1.49))
        assertEquals(2.5, streamWithStopAtInside.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.51, streamWithStopAtInside.valueAt(1.51) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, streamWithStopAtInside.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, streamWithStopAtInside.valueAt(3.1))
    }

    @Test
    fun withSampleAfter() {
        val stream = emptyValueStream
            .withSampleAfter(0.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .withSampleAfter(0.0, 3.0)
            .withSampleAfter(1.0, 4.0)
            .stop()
            .withSampleAfter(2.0, 6.0)

        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(1.0, stream.valueAt(0.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.5, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(3.99))
        assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(4.01))

        assertEquals(6.01, stream.withSampleAfter(1.0, 7.0).valueAt(4.01))

        val replaceStopStream = emptyValueStream
            .withSampleAfter(0.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(0.0, 3.0)
            .withSampleAfter(1.0, 4.0)

        assertEquals(2.0, replaceStopStream.valueAt(0.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, replaceStopStream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.5, replaceStopStream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)

        var correctException = false
        try {
            emptyValueStream.withSampleAfter(-0.1, 1.0)
        } catch (ex: IllegalArgumentException) {
            correctException = true
        }

        assertTrue(correctException)
    }

    @Test
    fun valueAtTest() {
        val stream = emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 5.0)
            .stop()
            .withSampleAfter(1.0, 6.0)

        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(null, stream.valueAt(0.5))
        assertEquals(1.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(5.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(4.01))
        assertEquals(null, stream.valueAt(4.5))
        assertEquals(null, stream.valueAt(4.99))
        assertEquals(6.0, stream.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(5.01))
        assertEquals(null, stream.valueAt(100.0))

        val interpolableStream = ValueStream.empty<Coordinates>()
            .withSampleAfter(1.0, Coordinates(1.0, 1.0))
            .withSampleAfter(1.0, Coordinates(2.0, 3.0))

        assertEquals(1.0, interpolableStream.valueAt(1.0)?.x ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, interpolableStream.valueAt(1.0)?.y ?: 0.0, DOUBLE_DELTA)

        assertEquals(1.3, interpolableStream.valueAt(1.3)?.x ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.6, interpolableStream.valueAt(1.3)?.y ?: 0.0, DOUBLE_DELTA)

        assertEquals(1.5, interpolableStream.valueAt(1.5)?.x ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, interpolableStream.valueAt(1.5)?.y ?: 0.0, DOUBLE_DELTA)

        assertEquals(2.0, interpolableStream.valueAt(2.0)?.x ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, interpolableStream.valueAt(2.0)?.y ?: 0.0, DOUBLE_DELTA)
    }

    @Test
    fun sectionTest() {
        val stream = emptyValueStream
            .stop()
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 5.0)
            .stop()
            .withSampleAfter(1.0, 6.0)
            .stop()

        val sectionA = stream.section(-10.0, 10.0)

        assertEquals(null, sectionA.valueAt(-0.5))
        assertEquals(null, sectionA.valueAt(0.9))
        assertEquals(1.5, sectionA.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionA.valueAt(2.01))
        assertEquals(null, sectionA.valueAt(2.99))
        assertEquals(4.5, sectionA.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionA.valueAt(4.01))
        assertEquals(null, sectionA.valueAt(4.99))
        assertEquals(6.0, sectionA.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionA.valueAt(5.1))
        assertEquals(null, sectionA.valueAt(10.0))

        val sectionB = stream.section(0.0, 2.0)

        assertEquals(null, sectionB.valueAt(-0.5))
        assertEquals(null, sectionB.valueAt(0.9))
        assertEquals(1.5, sectionB.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, sectionB.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionB.valueAt(2.01))
        assertEquals(null, sectionB.valueAt(3.1))
        assertEquals(null, sectionB.valueAt(4.01))
        assertEquals(null, sectionB.valueAt(5.0))
        assertEquals(null, sectionB.valueAt(5.1))
        assertEquals(null, sectionB.valueAt(10.0))
        assertTrue((sectionB.isStopped))

        val sectionC = stream.section(1.5, 3.5)

        assertEquals(null, sectionC.valueAt(-0.5))
        assertEquals(null, sectionC.valueAt(0.9))
        assertEquals(null, sectionC.valueAt(1.0))
        assertEquals(null, sectionC.valueAt(1.49))
        assertEquals(1.5, sectionC.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionC.valueAt(2.01))
        assertEquals(null, sectionC.valueAt(2.99))
        assertEquals(4.0, sectionC.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, sectionC.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, sectionC.valueAt(4.01))
        assertEquals(null, sectionC.valueAt(4.99))
        assertEquals(null, sectionC.valueAt(5.0))
        assertEquals(null, sectionC.valueAt(5.1))
        assertEquals(null, sectionC.valueAt(10.0))
        assertFalse(sectionC.isStopped)
    }

    @Test
    fun firstLastTest() {
        val stream = emptyValueStream
            .stop()
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 5.0)
            .stop()
            .withSampleAfter(1.0, 6.0)
            .stop()

        assertEquals(1.0, stream?.firstT ?: 0.0, DOUBLE_DELTA)
        assertEquals(5.0, stream?.lastT ?: 0.0, DOUBLE_DELTA)
        assertEquals(6.0, stream?.lastValue ?: 0.0, DOUBLE_DELTA)
    }

    @Test
    fun integralTest() {
        val stream = emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 1.0)
            .stop()
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 0.0)
            .withSampleAfter(1.0, 1.0)
            .stop()
            .withSampleAfter(1.0, 5.0)
            .integral()

        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(null, stream.valueAt(0.5))
        assertEquals(0.5, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(0.75, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(1.5, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.5, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.75, stream.valueAt(4.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(5.01))
        assertEquals(null, stream.valueAt(5.5))
        assertEquals(null, stream.valueAt(5.99))
        assertEquals(2.0, stream.valueAt(6.0))
        assertEquals(null, stream.valueAt(6.01))
        assertEquals(null, stream.valueAt(7.0))
        assertEquals(null, stream.valueAt(100.0))

        assertEquals(2.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        assertFalse(stream.isStopped)
    }

    @Test
    fun constantInterpolationTest() {
        val stream = emptyValueStream
            .withConstantInterpolation()
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 3.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 4.5)
            .withSampleAfter(1.0, 5.5)
            .stop()
            .withSampleAfter(1.0, 6.0)


        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(null, stream.valueAt(0.5))
        assertEquals(1.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.0, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, stream.valueAt(4.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(5.5, stream.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(5.01))
        assertEquals(null, stream.valueAt(5.99))
        assertEquals(6.0, stream.valueAt(6.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(7.0))
        assertEquals(null, stream.valueAt(100.0))

        assertEquals(6.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        assertFalse(stream.isStopped)
    }

    @Test
    fun constantInterpolationToLinearTest() {
        val stream = emptyValueStream
            .withConstantInterpolation()
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 3.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 4.5)
            .withSampleAfter(1.0, 5.5)
            .stop()
            .withSampleAfter(1.0, 6.0)
            .withLinearInterpolation()


        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(null, stream.valueAt(0.5))
        assertEquals(1.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(3.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.25, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(4.5, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(5.0, stream.valueAt(4.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(5.5, stream.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(5.01))
        assertEquals(null, stream.valueAt(5.99))
        assertEquals(6.0, stream.valueAt(6.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(7.0))
        assertEquals(null, stream.valueAt(100.0))

        assertEquals(6.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        assertFalse(stream.isStopped)
    }

    @Test
    fun derivativeTest() {
        val stream = emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 3.0)
            .stop()
            .withSampleAfter(1.0, 4.0)
            .withSampleAfter(1.0, 4.5)
            .withSampleAfter(1.0, 5.5)
            .stop()
            .withSampleAfter(1.0, 6.0)
            .derivative()

        assertEquals(null, stream.valueAt(-0.5))
        assertEquals(null, stream.valueAt(0.5))
        assertEquals(2.0, stream.valueAt(1.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(1.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(2.01))
        assertEquals(null, stream.valueAt(2.5))
        assertEquals(null, stream.valueAt(2.99))
        assertEquals(0.5, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(0.625, stream.valueAt(3.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(0.75, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(0.875, stream.valueAt(4.5) ?: 0.0, DOUBLE_DELTA)
        assertEquals(1.0, stream.valueAt(5.0) ?: 0.0, DOUBLE_DELTA)
        assertEquals(null, stream.valueAt(6.0))
        assertEquals(null, stream.valueAt(7.0))
        assertEquals(null, stream.valueAt(100.0))

        assertEquals(1.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        assertTrue(stream.isStopped)
    }

    @Test
    fun samplesTest() {
        val stream = emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 3.0)
            .withSampleAfter(1.0, 4.0)
            .stop()
            .withSampleAfter(1.0, 5.0)

        val dt = 0.1

        val samples = stream.sample(dt)

        // Check that all the samples are what they should be
        var expectedValue = 1.0
        for (sample in samples) {
            if ((expectedValue > 2.0 && expectedValue < 3.0) || (expectedValue > 4.0 && expectedValue != 5.0)) {
                assertEquals(null, sample)
            } else {
                assertEquals(expectedValue, sample ?: 0.0, DOUBLE_DELTA)
            }
            expectedValue += dt
        }

    }

    @Test
    fun lastValueTest() {
        assertEquals(null, emptyValueStream.lastValue)

        assertEquals(2.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0).lastValue)

        assertEquals(2.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0).stop().lastValue)

        assertEquals(3.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 3.0).lastValue)

        assertEquals(4.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(1.0, 3.0)
            .withSampleAfter(1.0, 4.0).lastValue)

        assertEquals(3.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .stop()
            .withSampleAfter(0.0, 3.0).lastValue)

        assertEquals(3.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .withSampleAfter(1.0, 3.0).lastValue)

        assertEquals(2.0, emptyValueStream
            .withSampleAfter(1.0, 1.0)
            .withSampleAfter(1.0, 2.0)
            .withSampleAt(-1.0, 3.0).lastValue)
    }

    @Test
    fun plusTest() {

        val shouldBeEmpty =
            emptyValueStream + emptyValueStream + emptyValueStream.stop() + emptyValueStream

        assertTrue(shouldBeEmpty.isEmpty)
        assertTrue(shouldBeEmpty.isStopped)
        assertEquals(null, shouldBeEmpty.lastValue)

        val streamA = emptyValueStream
            .withSampleAt(2.0, 2.0)
            .withSampleAt(4.0, 6.0)
            .stop()
            .withSampleAt(8.0, 8.0)
            .withSampleAt(10.0, 12.0)

        val streamB = emptyValueStream
            .withSampleAt(12.0, 10.0)
            .withSampleAt(14.0, 12.0)
            .withSampleAt(16.0, 14.0)

        fun testMergedStream(stream: ValueStream<Double>) {
            assertEquals(null, stream.valueAt(-1.0))
            assertEquals(null, stream.valueAt(-0.01))
            assertEquals(null, stream.valueAt(1.0))
            assertEquals(null, stream.valueAt(1.99))
            assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(4.01))
            assertEquals(null, stream.valueAt(7.99))
            assertEquals(8.0, stream.valueAt(8.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(9.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(10.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(11.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(12.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(13.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(14.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(13.0, stream.valueAt(15.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(14.0, stream.valueAt(16.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(16.01))
            assertEquals(null, stream.valueAt(17.0))
            assertEquals(null, stream.valueAt(100.0))

            assertFalse(stream.isStopped)
            assertFalse(stream.isEmpty)
            assertEquals(streamB.lastValue ?: 1.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        }

        testMergedStream(streamA + streamB)
        testMergedStream(streamB + streamA)

    }

    @Test
    fun plusStoppedTest() {

        val shouldBeEmpty =
            emptyValueStream + emptyValueStream + emptyValueStream.stop() + emptyValueStream

        assertTrue(shouldBeEmpty.isEmpty)
        assertTrue(shouldBeEmpty.isStopped)
        assertEquals(null, shouldBeEmpty.lastValue)

        val streamA = emptyValueStream
            .withSampleAt(2.0, 2.0)
            .withSampleAt(4.0, 6.0)
            .stop()
            .withSampleAt(8.0, 8.0)
            .withSampleAt(10.0, 12.0)
            .stop()

        val streamB = emptyValueStream
            .withSampleAt(12.0, 10.0)
            .withSampleAt(14.0, 12.0)
            .withSampleAt(16.0, 14.0)
            .stop()

        fun testMergedStream(stream: ValueStream<Double>) {
            assertEquals(null, stream.valueAt(-1.0))
            assertEquals(null, stream.valueAt(-0.01))
            assertEquals(null, stream.valueAt(1.0))
            assertEquals(null, stream.valueAt(1.99))
            assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(4.01))
            assertEquals(null, stream.valueAt(7.99))
            assertEquals(8.0, stream.valueAt(8.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(9.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(10.01))
            assertEquals(null, stream.valueAt(11.0))
            assertEquals(null, stream.valueAt(11.99))
            assertEquals(10.0, stream.valueAt(12.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(13.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(14.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(13.0, stream.valueAt(15.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(14.0, stream.valueAt(16.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(16.01))
            assertEquals(null, stream.valueAt(17.0))
            assertEquals(null, stream.valueAt(100.0))

            assertTrue(stream.isStopped)
            assertFalse(stream.isEmpty)
            assertEquals(streamB.lastValue ?: 1.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        }

        testMergedStream(streamA + streamB)
        testMergedStream(streamB + streamA)

    }

    @Test
    fun plusTouchingTest() {

        val shouldBeEmpty =
            emptyValueStream + emptyValueStream + emptyValueStream.stop() + emptyValueStream

        assertTrue(shouldBeEmpty.isEmpty)
        assertTrue(shouldBeEmpty.isStopped)
        assertEquals(null, shouldBeEmpty.lastValue)

        val streamA = emptyValueStream
            .withSampleAt(2.0, 2.0)
            .withSampleAt(4.0, 6.0)
            .stop()
            .withSampleAt(8.0, 8.0)
            .withSampleAt(10.0, 12.0)
            .withSampleAt(12.0, 14.0)

        val streamB = emptyValueStream
            .withSampleAt(12.0, 10.0)
            .withSampleAt(14.0, 12.0)
            .withSampleAt(16.0, 14.0)

        fun testMergedStream(stream: ValueStream<Double>) {
            assertEquals(null, stream.valueAt(-1.0))
            assertEquals(null, stream.valueAt(-0.01))
            assertEquals(null, stream.valueAt(1.0))
            assertEquals(null, stream.valueAt(1.99))
            assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(4.01))
            assertEquals(null, stream.valueAt(7.99))
            assertEquals(8.0, stream.valueAt(8.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(9.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(10.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(11.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(12.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(13.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(14.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(13.0, stream.valueAt(15.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(14.0, stream.valueAt(16.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(16.01))
            assertEquals(null, stream.valueAt(17.0))
            assertEquals(null, stream.valueAt(100.0))

            assertFalse(stream.isStopped)
            assertFalse(stream.isEmpty)
            assertEquals(streamB.lastValue ?: 1.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        }

        testMergedStream(streamA + streamB)
        testMergedStream(streamB + streamA)

    }

    @Test
    fun plusTouchingStoppedTest() {

        val shouldBeEmpty =
            emptyValueStream + emptyValueStream + emptyValueStream.stop() + emptyValueStream

        assertTrue(shouldBeEmpty.isEmpty)
        assertTrue(shouldBeEmpty.isStopped)
        assertEquals(null, shouldBeEmpty.lastValue)

        val streamA = emptyValueStream
            .withSampleAt(2.0, 2.0)
            .withSampleAt(4.0, 6.0)
            .stop()
            .withSampleAt(8.0, 8.0)
            .withSampleAt(10.0, 12.0)
            .withSampleAt(12.0, 14.0)
            .stop()

        val streamB = emptyValueStream
            .withSampleAt(12.0, 10.0)
            .withSampleAt(14.0, 12.0)
            .withSampleAt(16.0, 14.0)

        fun testMergedStream(stream: ValueStream<Double>, reversed: Boolean) {
            assertEquals(null, stream.valueAt(-1.0))
            assertEquals(null, stream.valueAt(-0.01))
            assertEquals(null, stream.valueAt(1.0))
            assertEquals(null, stream.valueAt(1.99))
            assertEquals(2.0, stream.valueAt(2.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(4.0, stream.valueAt(3.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(6.0, stream.valueAt(4.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(4.01))
            assertEquals(null, stream.valueAt(7.99))
            assertEquals(8.0, stream.valueAt(8.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(9.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(10.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(11.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(10.0, stream.valueAt(12.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(11.0, stream.valueAt(13.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(12.0, stream.valueAt(14.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(13.0, stream.valueAt(15.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(14.0, stream.valueAt(16.0) ?: 0.0, DOUBLE_DELTA)
            assertEquals(null, stream.valueAt(16.01))
            assertEquals(null, stream.valueAt(17.0))
            assertEquals(null, stream.valueAt(100.0))

            assertFalse(stream.isStopped)
            assertFalse(stream.isEmpty)
            assertEquals(streamB.lastValue ?: 1.0, stream.lastValue ?: 0.0, DOUBLE_DELTA)
        }

        testMergedStream(streamA + streamB, false)
        testMergedStream(streamB + streamA, true)

    }

    class Coordinates(val x: Double, val y: Double): ValueStream.Interpolable {
        override fun interpolate(
            other: ValueStream.Interpolable,
            factor: Double
        ): ValueStream.Interpolable {

            if (other is Coordinates) {
                return Coordinates(
                    x * (1.0 - factor) + other.x * factor,
                    y * (1.0 - factor) + other.y * factor)
            } else {
                return this
            }

        }

    }
}